# Hundir la flota

En esta práctica de la asignatura **Sistemas multiagente** se realiza la implementación del clásico juego de mesa *[Hundir la flota](https://www.planinfantil.es/plan/hundir-la-flota/)* mediante el uso de protocolos de paso de mensajes y ontología.

## Análisis y diseño del juego

Este es un juego de dos jugadores. Cada jugador tiene un tablero casillas en el que colocar diferentes barcos de distintos tamaños. El número de casillas y el número y tamaño de barcos es adaptable a las reglas *de cada casa* pero en este caso se ha optado por un tablero de 10x10 y:

 - 1 barco de 4 casillas 
 - 2 barcos de 3 casillas 
 - 3 barcos de 2 casillas 
 - 4 barcos de 1 casilla
 
Estos parámetros son configurables desde el archivo constantes (insertar enlace).

El transcurso de la partida se llevará a cabo entre dos agentes jugador y un agente tablero.
Para empezar la partida, el tablero le pedirá al jugador que ponga sus barcos en el tablero de la forma que desee. En este caso se hace aleatoriamente, de forma que los barcos no se superpongan unos con otros y dejen al menos una casilla de distancia, por lo que tampoco pueden tocarse.
Una vez el tablero tenga constancia de la colocación de los barcos de cada jugador dará comienzo la ronda de turnos. El tablero le pedirá a un jugador que lance un torpedo a la casilla deseada del tablero contrario, y este le informará del resultado de su operación, que puede ser TORPEDO_AGUA o TORPEDO_BARCO, dependiendo de si en esa casilla había un barco o no.
Una vez terminado este procedimiento le pedirá que lance un torpedo al otro jugador. Y así sucesivamente.

La partida se terminará cuando uno de los jugadores se declare ganador, es decir, ha hundido todos los barcos del jugador contrario.

# Agentes

## Agente Monitor
La implementación de este agente ha sido realizada por el profesor, así que hablaré brevemente de sus funciones:
- Buscar todos los agentes relacionados con un juego activos en la plataforma.
- Creación de juegos: para ello necesita al menos un agente monitor y n agentes jugador disponibles. Un juego puede ser de tipo UNICO (partida única, solo son necesarios 2 jugadores), TORNEO o LIGA (no se implementan en esta práctica, pero necesitarían 8 jugadores).
- Se suscribe a los agentes monitor a los que haya encargado un juego para recibir los resultados de este.
### Interfaces
### Protocolos
[Proponer a los diferentes jugadores que participen en un juego. (Iniciador)](https://gitlab.com/ssmmaa/ontojuegos#12-c%C3%B3mo-proponer-a-los-diferentes-jugadores-que-participen-en-un-juego)
Protocolo propose para pedir al jugador que participe en un juego, este puede contestar afirmativa o negativamente.
[Obtener el resultado del juego propuesto. (Iniciador)](https://gitlab.com/ssmmaa/ontojuegos#14-c%C3%B3mo-obtener-el-resultado-del-juego-propuesto)
El agente monitor se suscribe al agente organizador para que, una vez finalizado el juego, le informe del resultado del juego una vez finalizado este.

## Agente Organizador
Este agente se encarga de crear las partidas que componen un juego. Las partidas se las manda el agente organizador, así que ya consta de los jugadores que van a jugar ese juego. Como ya he comentado, en este caso solo se harán juegos de tipo UNICO, por lo que el organizador solo tendrá que crear una partida por juego.
Sus funciones:
- Buscar todos los agentes tablero relacionados con un juego activos en la plataforma.
- Una vez tenga un agente tablero dispuesto a jugar la partida, le indicará que de comienzo.
- Se suscribe a los agentes tablero a los que haya encargado una partida para recibir los resultados de esta.
### Interfaces
### Protocolos
[Completar un juego. (Participante)](https://gitlab.com/ssmmaa/ontojuegos#13-c%C3%B3mo-debe-completarse-un-juego-)
Recibe una proposición del agente monitor para completar un juego, puede responder aceptar o denegar la proposición.
[Obtener el resultado del juego propuesto. (Participante)](https://gitlab.com/ssmmaa/ontojuegos#14-c%C3%B3mo-obtener-el-resultado-del-juego-propuesto)
Si acepta la petición de suscribe de parte del agente monitor, cuando tenga el resultado del juego le avisará mediante un INFORM. Podrá enviarle la clasificación del juego en caso de que todo haya ido correctamente o la incidencia juego en caso de que ocurriera.
[Generar partidas que componen un juego. (Iniciador)](https://gitlab.com/ssmmaa/ontojuegos#15-c%C3%B3mo-generar-las-partidas-que-componen-un-juego)
El organizador crea las partidas y se las propone a un tablero para que puedan dar inicio.
[Obtener el resultado de la partida propuesta. (Iniciador)](https://gitlab.com/ssmmaa/ontojuegos#17-c%C3%B3mo-informar-del-resultado-final-de-la-partida)
El agente organizador se suscribe al agente tablero que maneja las partidas que el ha creado, para que una vez finalizada la partida le informe del resultado.


## Agente Tablero
Una vez recibida la partida por parte del agente organizador, el agente tablero se encarga del transcurso de la partida.
Sus funciones:
- Recibir las partidas por parte del agente organizador.
- Pedir movimientos a los jugadores y aplicarlos dentro de su estructura de tablero.
- Devolver el resultado del movimiento al jugador lanzador.
- Informar a los agentes monitores y jugadores del resultado de la partida.
### Interfaces
### Protocolos
[Generar partidas que componen un juego. (Participante)](https://gitlab.com/ssmmaa/ontojuegos#15-c%C3%B3mo-generar-las-partidas-que-componen-un-juego)
El agente tablero recibe una petición de parte de un agente organizador para que se encargue de la partida propuesta. Puede responder afirmativa o negativamente a la propuesta.

[Turno para juego. (Iniciador)](https://gitlab.com/ssmmaa/ontojuegos#16-c%C3%B3mo-completar-un-turno-de-una-partida-c%C3%B3mo-completar-la-partida)
El agente tablero solicita al agente jugador la acción de PedirMovimiento. Puede recibir un REFUSE indicando que el jugador abandona la partida, o un PROPOSE con un movimiento (de parte del jugador que le toca lanzar el torpedo) o un estado de la partida (de parte del otro jugador).
Una vez recibido el movimiento el tablero lo aplica y devuelve el resultado del lanzamiento a los dos agentes jugadores para que lo apliquen ellos.
Por ultimo recibirá un INFORM en el que los jugadores deciden seguir jugando o alguno ya se ha declarado ganador.
[Informar del resultado de la partida. (Participante)](https://gitlab.com/ssmmaa/ontojuegos#17-c%C3%B3mo-informar-del-resultado-final-de-la-partida)
Recibe peticiones de suscripción de parte de los agentes organizador y jugador, que son los que quieren saber el resultado de las partidas. Este responderá con un AGREE en caso de aceptar la suscripción. Cuando la partida haya finalizado informará del resultado a los agente suscritos.

## Agente Jugador
El agente jugador será el encargado de ir jugando las partidas de los juego que haya aceptado, para ello se comunicará primero con el agente monitor para aceptar o no el juego, y después con el agente tablero para iniciar la partida (colocación de barcos) y responder a las peticiones de movimiento que este le pueda hacer. Él mismo será el encargado de declararse ganador en el momento que haya hundido todos los barcos de su rival. Esto se hace así suponiendo que el jugador no puede hacer trampa, pero sería mejor que el agente tablero declarara el ganador.
Sus funciones:
- Atender las peticiones del agente monitor de jugar a un juego.
- Inicializar el tablero de juego cuando se lo pida el agente tablero.
- Lanzar un torpedo a la petición del agente tablero.
- Actualizar su estructura de datos e interfaz gráfica con los resultados de su lanzamiento.
- Revisa si es ganador de la partida y lo informa al agente tablero.
### Interfaces
### Protocolos
[Proposición de juego. (Participante)](https://gitlab.com/ssmmaa/ontojuegos#12-c%C3%B3mo-proponer-a-los-diferentes-jugadores-que-participen-en-un-juego)
El agente jugador recibe un PROPOSE de parte del agente monitor para participar en un juego. El jugador puede responder afirmativa o negativamente.
[Completar turno de partida. (Participante)](https://gitlab.com/ssmmaa/ontojuegos#16-c%C3%B3mo-completar-un-turno-de-una-partida-c%C3%B3mo-completar-la-partida)
El agente jugador recibe de parte del agente tablero un CFP pidiéndole que realice un movimiento. En caso de no ser el turno del jugador este devolverá un PROPOSE con el estado de la partida, simplemente para hacerle saber que quiere seguir jugando, o el movimiento pedido en caso de sí ser su turno. En cualquiera de los casos el jugador puede decir que no quiere seguir jugando mediante un REFUSE.
Una vez el jugador al que le toque disparar haya mandado su movimiento, ambos jugadores recibirán el resultado del disparo y devolverán el estado de la partida. En caso de que uno de ellos se proclame ganador se lo hará saber al agente tablero mediante el mensaje de INFORM.
[Informarse del resultado de la partida. (Iniciador)](https://gitlab.com/ssmmaa/ontojuegos#17-c%C3%B3mo-informar-del-resultado-final-de-la-partida)
Los jugadores quieren saber el resultado de las partidas que están jugando una vez hayan finalizado. Para ello enviarán un mensaje suscribe a los tableros encargados de sus partidas para que les informe del resultado, tanto si hay un ganador como si ha ocurrido un error durante la partida.




# Protocolos específicos

## Petición de inicio de partida

Para iniciar una partida, el agente tablero debe pedirle a los jugadores que le envíen su colocación de barcos antes de empezar los movimientos, ya que si no tiene constancia de ellos no puede saber el resultado de los movimientos. Para ello se hará uso del protocolo **REQUEST**.

```mermaid
sequenceDiagram
Agente tablero->> Agente jugador: REQUEST (PedirBarcos)
Agente jugador-->>Agente tablero: REFUSE (EstadoPartida)
Agente jugador-->>Agente tablero: INFORM (BarcosColocados)
```