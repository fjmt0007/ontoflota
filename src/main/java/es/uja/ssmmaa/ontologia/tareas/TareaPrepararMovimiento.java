/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.Constantes;
import es.uja.ssmmaa.ontologia.Constantes.TipoCasilla;
import static es.uja.ssmmaa.ontologia.Constantes.TipoCasilla.DESCONOCIDO;
import static es.uja.ssmmaa.ontologia.Constantes.TipoCasilla.TORPEDO_BARCO;
import es.uja.ssmmaa.ontologia.agentes.AgenteJugador;
import es.uja.ssmmaa.ontologia.hundirlaflota.PartidaHundirLaFlota;
import es.uja.ssmmaa.ontologia.juegoTablero.Posicion;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Javi
 */
public class TareaPrepararMovimiento extends OneShotBehaviour {

    AgenteJugador agente;
    String idPartida;
    TipoCasilla[][] tablero;
    ArrayList<Posicion> desconocidos;

    public TareaPrepararMovimiento(Agent a, TipoCasilla[][] tablero, String idPartida) {
        super(a);
        agente = (AgenteJugador) a;
        this.tablero = tablero;
        this.idPartida = idPartida;
    }

    @Override
    public void action() {
        obtenerDesconocidos(); //Paso esencial para no repetir casillas
        Posicion p = inteligencia();
        if (p == null) {
            p = aleatorio();
        }
        agente.addMovimiento(idPartida, p);
    }

    private Posicion aleatorio() {
        Random r = Constantes.aleatorio;
        Posicion pos = desconocidos.get(r.nextInt(desconocidos.size()));

        return pos;
    }

    private void obtenerDesconocidos() {
        desconocidos = new ArrayList<>();
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                if (tablero[i][j] == DESCONOCIDO) {
                    Posicion p = new Posicion(i, j);
                    desconocidos.add(p);
                }
            }
        }
    }

    private Posicion inteligencia() {
        Posicion p = null;
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero.length; j++) {
                if (tablero[i][j] == TORPEDO_BARCO) {
                    p = probarPosiciones(i, j);
                    if (p != null) {
                        return p;
                    }
                }
            }
        }
        return p;
    }

    private boolean isDesconocida(Posicion p) {
        for (int i = 0; i < desconocidos.size(); i++) {
            if (p.equals(desconocidos.get(i))) {
                return true;
            }
        }
        return false;
    }

    private TipoCasilla derecha(int i, int j) {
        if (i + 1 < tablero.length) {
            return tablero[i + 1][j];
        }
        return DESCONOCIDO;
    }

    private TipoCasilla izquierda(int i, int j) {
        if (i - 1 >= 0) {
            return tablero[i - 1][j];
        }
        return DESCONOCIDO;
    }

    private TipoCasilla arriba(int i, int j) {
        if (j - 1 >= 0) {
            return tablero[i][j - 1];
        }
        return DESCONOCIDO;
    }

    private TipoCasilla abajo(int i, int j) {
        if (j + 1 < tablero.length) {
            return tablero[i][j + 1];
        }
        return DESCONOCIDO;
    }

    private Posicion probarPosiciones(int i, int j) {
        Posicion p;
        ArrayList<Posicion> validas = new ArrayList();
        int direccion = -1; //0 horizontal y 1 vertical

        //Obtenemos la direccion si la hubiera
        if (derecha(i, j).equals(TORPEDO_BARCO) || izquierda(i, j).equals(TORPEDO_BARCO)) {
            direccion = 0;
        } else if (arriba(i, j).equals(TORPEDO_BARCO) || abajo(i, j).equals(TORPEDO_BARCO)) {
            direccion = 1;
        }

        if (direccion != 1) {
            //Miramos a la derecha
            if (i + 1 < tablero.length) {
                p = new Posicion(i - 1, j); //Disparamos a la izquierda
                if (isDesconocida(p)) {
                    validas.add(p);
                    if (tablero[i + 1][j] == TORPEDO_BARCO) {
                        return p;
                    }
                }
            }

            //Miramos a la izquierda
            if (i - 1 >= 0) {
                p = new Posicion(i + 1, j); //Disparamos a la derecha
                if (isDesconocida(p)) {
                    validas.add(p);
                    if (tablero[i - 1][j] == TORPEDO_BARCO) {
                        return p;
                    }
                }
            }
        }

        if (direccion != 0) {
            //Miramos abajo
            if (j + 1 < tablero.length) {
                p = new Posicion(i, j - 1); //Disparamos arriba
                if (isDesconocida(p)) {
                    validas.add(p);
                    if (tablero[i][j + 1] == TORPEDO_BARCO) {
                        return p;
                    }
                }
            }

            //Miramos arriba
            if (j - 1 >= 0) {
                p = new Posicion(i, j + 1); //Disparamos abajo
                if (isDesconocida(p)) {
                    validas.add(p);
                    if (tablero[i][j + 1] == TORPEDO_BARCO) {
                        return p;
                    }
                }
            }
        }

        //Si no ha encontrado ningun sentido claro cogemos uno aleatorio
        if (validas.size() > 0) {
            return validas.get(Constantes.aleatorio.nextInt(validas.size()));
        }

        //Si ninguno de los casos anteriores ha resultado devuelve null y busca aleatoriamente
        return null;

    }

}
