/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.agentes.AgenteJugador;
import es.uja.ssmmaa.ontologia.hundirlaflota.BarcosColocados;
import es.uja.ssmmaa.ontologia.hundirlaflota.PedirBarcos;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.INFORM;
import static jade.lang.acl.ACLMessage.REFUSE;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javi
 */
public class TareaPedirBarcosParticipante extends AchieveREResponder {

    AgenteJugador agente;
    ContentManager manager;

    public TareaPedirBarcosParticipante(Agent a, MessageTemplate mt) {
        super(a, mt);
        agente = (AgenteJugador) a;
        manager = agente.getContentManager();
    }

    @Override
    protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
        //System.out.println("Recibiendo peticion de colocacion" + request);
        try {
            Action accion = (Action) manager.extractContent(request);
            PedirBarcos pedirBarcos = (PedirBarcos) accion.getAction();

            BarcosColocados bc = agente.inicializarPartida(pedirBarcos);

            //Contestamos al request con un inform
            ACLMessage contestar = request.createReply();
            if (bc != null) {
                contestar.setPerformative(INFORM);
                manager.fillContent(contestar, bc);
            }else{
                contestar.setPerformative(REFUSE);
            }

            return contestar;

        } catch (Codec.CodecException ex) {
            Logger.getLogger(TareaPedirBarcosParticipante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(TareaPedirBarcosParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    

}
