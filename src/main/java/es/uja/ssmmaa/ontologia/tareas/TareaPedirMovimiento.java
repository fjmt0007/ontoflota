/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.Constantes.TipoCasilla;
import es.uja.ssmmaa.ontologia.Vocabulario;
import static es.uja.ssmmaa.ontologia.Vocabulario.Estado.GANADOR;
import static es.uja.ssmmaa.ontologia.Vocabulario.Estado.SEGUIR_JUGANDO;
import es.uja.ssmmaa.ontologia.agentes.AgenteTablero;
import es.uja.ssmmaa.ontologia.hundirlaflota.Peticion;
import es.uja.ssmmaa.ontologia.hundirlaflota.Torpedo;
import es.uja.ssmmaa.ontologia.juegoTablero.EstadoPartida;
import es.uja.ssmmaa.ontologia.juegoTablero.MovimientoEntregado;
import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.ACCEPT_PROPOSAL;
import jade.proto.ContractNetInitiator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Marin
 */
public class TareaPedirMovimiento extends ContractNetInitiator {

    private final AgenteTablero agente;
    private final ContentManager manager;
    private AID proximoLanzador;
    private AID lanzadorActual;

    public TareaPedirMovimiento(Agent a, ACLMessage cfp) {
        super(a, cfp);
        agente = (AgenteTablero) a;
        manager = agente.getContentManager();
    }

    @Override
    protected void handleOutOfSequence(ACLMessage msg) {
        //System.out.println("Tablero: ha llegado un mensaje fuera de secuencia");
    }

    @Override
    protected void handleInform(ACLMessage inform) {
        ContentElement respuesta;
        try {
            respuesta = manager.extractContent(inform);
            if (respuesta instanceof EstadoPartida) {
                EstadoPartida estado = (EstadoPartida) respuesta;
                if (estado.getEstadoPartida() == SEGUIR_JUGANDO && inform.getSender().compareTo(lanzadorActual)==0) {
                    Peticion peticion = new Peticion(estado.getPartida().getIdPartida(), proximoLanzador);
                    //agente.pedirMovimiento(peticion);
                    agente.colaPeticion(peticion);
                }else if (estado.getEstadoPartida() == GANADOR && inform.getSender().compareTo(lanzadorActual)==0){
                    System.out.println("Se declara ganador " + inform.getSender().getLocalName());
                    //----------------------------------------------------------------------------------------------tengo que desocupar el agente tablero
                }
            } else {
                System.out.println("Error de mensaje");

            }

        } catch (Codec.CodecException ex) {
            Logger.getLogger(TareaPedirMovimiento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(TareaPedirMovimiento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void handleAllResponses(Vector responses, Vector acceptances) {
        ACLMessage mensaje;
        Iterator it = responses.iterator();
        ArrayList<ACLMessage> emisores = new ArrayList<>();
        MovimientoEntregado movimiento = null;
        while (it.hasNext()) {
            ACLMessage msg = (ACLMessage) it.next();
            emisores.add(msg);

            try {
                ContentElement respuesta = manager.extractContent(msg);
                switch (msg.getPerformative()) {
                    case ACLMessage.PROPOSE:
                        //Recibimos un movimiento entregado
                        if (respuesta instanceof MovimientoEntregado) {
                            //Preparamos la respuesta, aplicando el moviminento y devolviendo el resultado
                            movimiento = (MovimientoEntregado) respuesta;
                            TipoCasilla resultado = agente.aplicarMovimiento(movimiento);
                            Torpedo t = (Torpedo)movimiento.getMovimiento().getFicha();
                            t.setTipoCasilla(resultado);
                            lanzadorActual = msg.getSender();
                        } //Recibimos un estado de la partida
                        else if (respuesta instanceof EstadoPartida) {
                            EstadoPartida estado = (EstadoPartida) respuesta;
                            proximoLanzador = msg.getSender();
                        } else {
                            System.out.println("Error de mensaje");
                        }
                        break;
                    case ACLMessage.REFUSE:
                        if (respuesta instanceof EstadoPartida) {
                            EstadoPartida estado = (EstadoPartida) respuesta;
                        } else {
                            System.out.println("Error de mensaje");

                        }
                        break;

                    default:
                        System.out.println("Tipo de mensaje no esperado");

                }

            } catch (Codec.CodecException ex) {
                Logger.getLogger(TareaPedirMovimiento.class.getName()).log(Level.SEVERE, null, ex);
            } catch (OntologyException ex) {
                Logger.getLogger(TareaPedirMovimiento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Respondemos a los jugadores
        for (int i = 0; i < emisores.size(); i++) {
            ACLMessage respuesta = emisores.get(i).createReply();
            respuesta.setPerformative(ACCEPT_PROPOSAL);
            try {
                manager.fillContent(respuesta, movimiento);
            } catch (Codec.CodecException ex) {
                Logger.getLogger(TareaPedirMovimiento.class.getName()).log(Level.SEVERE, null, ex);
            } catch (OntologyException ex) {
                Logger.getLogger(TareaPedirMovimiento.class.getName()).log(Level.SEVERE, null, ex);
            }
            acceptances.add(respuesta);
        }


    }
}
