/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.agentes.AgenteTablero;
import es.uja.ssmmaa.ontologia.hundirlaflota.BarcosColocados;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javi
 */
public class TareaPedirBarcos extends AchieveREInitiator{
    
    AgenteTablero agente;
    ContentManager manager;
    
    public TareaPedirBarcos(Agent a, ACLMessage msg) {
        super(a, msg);
        agente = (AgenteTablero) a;
        manager = agente.getManager();
        //System.out.println("Enviando peticion de colocacion" + msg);
    }

    @Override
    protected void handleRefuse(ACLMessage refuse) {
        //El jugador no me da los barcos asi que termina la partida por abandono
    }

    @Override
    protected void handleInform(ACLMessage inform) {
        System.out.println("Recibiendo barcos");
        try {
            //Recibo los barcos
            BarcosColocados barcosC=(BarcosColocados) manager.extractContent(inform);
            agente.colocarBarcos(barcosC);
        } catch (Codec.CodecException ex) {
            Logger.getLogger(TareaPedirBarcos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(TareaPedirBarcos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    
}
