/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.Vocabulario;
import es.uja.ssmmaa.ontologia.juegoTablero.AgenteJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.InformarResultado;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.JuegoAceptado;
import es.uja.ssmmaa.ontologia.juegoTablero.Justificacion;
import es.uja.ssmmaa.ontologia.juegoTablero.Monitor;
import es.uja.ssmmaa.ontologia.util.GestorSubscripciones;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionResponder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Marin
 */
public class TareaInformarResultadoParticipante extends SubscriptionResponder {

    private final ContentManager manager;
    private final Agent agente;
    private final GestorSubscripciones gestor;

    public TareaInformarResultadoParticipante(Agent a, MessageTemplate mt, GestorSubscripciones gestor, ContentManager m) {
        super(a, mt, gestor);
        agente = a;
        manager = m;
        this.gestor = gestor;
    }

    @Override
    protected ACLMessage handleSubscription(ACLMessage subscription) throws NotUnderstoodException, RefuseException {
        ACLMessage respuesta = subscription.createReply();
        try {
            analizarMensaje(subscription, respuesta);
        } catch (Codec.CodecException ex) {
            Logger.getLogger(TareaInformarResultadoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(TareaInformarResultadoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }
        return respuesta;
    }

    private void analizarMensaje(ACLMessage subscription, ACLMessage respuesta) throws Codec.CodecException, OntologyException, RefuseException, NotUnderstoodException {
        Action accion = (Action) manager.extractContent(subscription);
        if (accion.getAction() instanceof InformarResultado) {
            InformarResultado informarResultado = (InformarResultado) accion.getAction();
            Monitor monitor = (Monitor) informarResultado.getAgente();
            Juego juego = new Juego("3", Vocabulario.TipoJuego.HUNDIR_LA_FLOTA);

            Justificacion justificacion = new Justificacion(juego, Vocabulario.Motivo.SUBSCRIPCION_ACEPTADA);

            aceptar(subscription, respuesta, justificacion);

        }
    }

    private void aceptar(ACLMessage subscription, ACLMessage respuesta, Justificacion justificacion) throws Codec.CodecException, OntologyException, RefuseException, NotUnderstoodException {
        respuesta.setPerformative(ACLMessage.AGREE);
        manager.fillContent(respuesta, justificacion);

        Subscription suscripcion = createSubscription(subscription);
        gestor.register(suscripcion);

    }

}
