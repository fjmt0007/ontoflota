/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.JUGADOR;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.JuegoAceptado;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Justificacion;
import es.uja.ssmmaa.ontologia.juegoTablero.Organizador;
import es.uja.ssmmaa.ontologia.util.RegistroJuego;

import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.ACCEPT_PROPOSAL;
import static jade.lang.acl.ACLMessage.REJECT_PROPOSAL;
import jade.proto.ProposeInitiator;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author pedroj
 */
public class TareaProponerJuego extends ProposeInitiator {

    private final TareasJuego agente;
    private final Juego juego;
    private final boolean proponerPartida;
    private final ContentManager manager;

    public TareaProponerJuego(Agent a, ACLMessage msg, Juego juego, TipoServicio tipoServicio) {
        super(a, msg);

        this.agente = (TareasJuego) a;
        this.juego = juego;
        this.proponerPartida = tipoServicio.equals(JUGADOR);
        this.manager = agente.getManager(juego.getTipoJuego());

    }

    @Override
    protected void handleOutOfSequence(ACLMessage msg) {
        // Ha llegado un mensaje fuera de la secuencia del protocolo
        agente.addMsgConsola("ERROR en Proponer Juego_________________\n" + msg);
    }

    @Override
    protected void handleAllResponses(Vector responses) {
        String idJuego = juego.getIdJuego();
        RegistroJuego registroJuego = agente.getRegistroJuego(idJuego);
        AID agenteJuego = null;
        Iterator it = responses.iterator();

        while (it.hasNext()) {
            ACLMessage msg = (ACLMessage) it.next();

            agenteJuego = msg.getSender();
            try {
                ContentElement respuesta = manager.extractContent(msg);

                switch (msg.getPerformative()) {
                    case ACCEPT_PROPOSAL:
                        JuegoAceptado juegoAceptado = (JuegoAceptado) respuesta;
                        if (proponerPartida) {
                            Jugador jugador = (Jugador) juegoAceptado.getAgenteJuego();
                            registroJuego.addJugador(jugador);
                            agente.addMsgConsola("Acepta el juego : " + juego
                                    + "\n" + jugador);
                        } else {
                            
                            Organizador organizador = (Organizador) juegoAceptado.getAgenteJuego();
                            agente.addMsgConsola("Acepta completar : " + juego
                                    + "\nEl Organizador:" + organizador);
                            agente.addJuego(juego, organizador);
                        }
                        break;

                    case REJECT_PROPOSAL:
                        Justificacion justificacion = (Justificacion) respuesta;
                        agente.addMsgConsola("El agente del juego " + agenteJuego
                                + "\nRechaza el  " + justificacion.getJuego()
                                + "\nJustificación: " + justificacion.getDetalle());
                        break;

                    default:
                        agente.addMsgConsola("EL agente " + agenteJuego.getLocalName()
                                + " NO ENTIENDE EL MENSAJE");
                }
            } catch (Codec.CodecException | OntologyException ex) {
                agente.addMsgConsola("ERROR en la construcción del mensaje de\n"
                        + agenteJuego.getLocalName() + "\n" + msg + "\n" + ex);
            }
        }

        if (proponerPartida) {
            agente.setRegistroJuego(idJuego, registroJuego);
            agente.addMsgConsola("Registro de: " + juego
                    + "\nEstado: " + registroJuego.registroCompleto()
                    + "\nNúmero de jugadores: " + registroJuego.numJugadores());
        } else if (responses.isEmpty()) {
            agente.addMsgConsola("NO HAY RESPUESTA DEL ORGANIZADOR\n" + juego);
        }
    }
}
