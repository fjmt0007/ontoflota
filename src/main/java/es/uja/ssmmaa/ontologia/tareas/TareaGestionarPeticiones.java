/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.agentes.AgenteTablero;
import es.uja.ssmmaa.ontologia.hundirlaflota.Peticion;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javi
 */
public class TareaGestionarPeticiones extends CyclicBehaviour {

    AgenteTablero agente;

    public TareaGestionarPeticiones(Agent a) {
        super(a);
        this.agente = (AgenteTablero) a;
    }

    @Override
    public void action() {
        if (!agente.isOcupado()) {
            Peticion peticion = agente.getProximaPeticion();
            if (peticion != null) {
                try {
                    agente.setOcupado(true);
                    agente.pedirMovimiento(peticion);
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(TareaGestionarPeticiones.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(TareaGestionarPeticiones.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
