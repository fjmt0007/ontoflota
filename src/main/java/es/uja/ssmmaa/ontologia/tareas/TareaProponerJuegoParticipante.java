/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.hundirlaflota.PartidaHundirLaFlota;
import es.uja.ssmmaa.ontologia.Vocabulario;
import es.uja.ssmmaa.ontologia.Vocabulario.Motivo;
import es.uja.ssmmaa.ontologia.Vocabulario.TipoJuego;
import es.uja.ssmmaa.ontologia.agentes.AgenteJugador;
import es.uja.ssmmaa.ontologia.agentes.AgenteOrganizador;
import es.uja.ssmmaa.ontologia.agentes.AgenteTablero;
import es.uja.ssmmaa.ontologia.hundirlaflota.HundirLaFlota;
import es.uja.ssmmaa.ontologia.juegoTablero.AgenteJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.CompletarJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.CompletarPartida;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.JuegoAceptado;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Justificacion;
import es.uja.ssmmaa.ontologia.juegoTablero.Monitor;
import es.uja.ssmmaa.ontologia.juegoTablero.Organizador;
import es.uja.ssmmaa.ontologia.juegoTablero.ProponerJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Tablero;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.abs.AbsContentElement;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ProposeResponder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Marin
 */
public class TareaProponerJuegoParticipante extends ProposeResponder {
    
    private final ContentManager manager;
    private final Agent agente;
    
    public TareaProponerJuegoParticipante(Agent a, MessageTemplate mt, ContentManager m) {
        super(a, mt);
        this.agente = a;
        manager = m;
    }
    
    @Override
    protected ACLMessage prepareResponse(ACLMessage propose) throws NotUnderstoodException, RefuseException {
        ACLMessage respuesta = propose.createReply();
        try {
            analizarMensaje(propose, respuesta);
            
        } catch (Codec.CodecException ex) {
            Logger.getLogger(TareaProponerJuegoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(TareaProponerJuegoParticipante.class.getName()).log(Level.SEVERE, null, ex);
            //rechazar(respuesta, proposicion, Motivo.ONTOLOGIA_DESCONOCIDA);
        }
        
        return respuesta;
    }
    
    private void analizarMensaje(ACLMessage propose, ACLMessage respuesta) throws Codec.CodecException, OntologyException {
        Action accion = (Action) manager.extractContent(propose);
        //Es un mensaje para AgenteJugador
        if (agente instanceof AgenteJugador) {
            AgenteJugador agenteJugador = (AgenteJugador) agente;
            
            if (accion.getAction() instanceof ProponerJuego) {
                ProponerJuego proponerJuego = (ProponerJuego) accion.getAction();
                Juego juego = proponerJuego.getJuego();
                Jugador jugador = new Jugador(agente.getName(), agente.getAID());
                aceptar(respuesta, juego, jugador);
                //Incluimos el juego en el agente
                agenteJugador.addPartida(juego);
            }
        }
        //Es un mensaje para AgenteOrganizador
        if (accion.getAction() instanceof CompletarJuego && agente instanceof AgenteOrganizador) {
            CompletarJuego completarJuego = (CompletarJuego) accion.getAction();
            Juego juego = completarJuego.getJuego();
            Organizador organizador = new Organizador(agente.getName(), agente.getAID());
            aceptar(respuesta, juego, organizador);

            //Incluimos el juego en el agente
            AgenteOrganizador agenteOrganizador = (AgenteOrganizador) agente;
            agenteOrganizador.addJuego(juego, completarJuego);
            
        }
        //Es un mensaje para AgenteTablero  ------------------------------------------------------------sin terminar
        if (accion.getAction() instanceof CompletarPartida) {
            CompletarPartida completarPartida = (CompletarPartida) accion.getAction();
            Juego juego = completarPartida.getPartida().getJuego();
            Organizador tab = new Organizador(agente.getName(), agente.getAID());
            AgenteTablero at = (AgenteTablero) agente;
            at.addPartida(completarPartida);
            aceptar(respuesta, juego, tab);
        }
    }
    
    private void aceptar(ACLMessage respuesta, Juego juego, AgenteJuego agente) throws Codec.CodecException, OntologyException {
        //Completamos nuestra respuesta
        respuesta.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
        JuegoAceptado juegoAceptado = new JuegoAceptado(juego, agente);
        
        manager.fillContent(respuesta, juegoAceptado);
    }
    
    private void rechazar(ACLMessage respuesta, ProponerJuego proposicion, Motivo motivo) throws Codec.CodecException, OntologyException {
        respuesta.setPerformative(ACLMessage.REJECT_PROPOSAL);
        Juego juego = proposicion.getJuego();
        Justificacion justificacion = new Justificacion(juego, motivo);
        manager.fillContent(respuesta, justificacion);
        
    }
    
}
