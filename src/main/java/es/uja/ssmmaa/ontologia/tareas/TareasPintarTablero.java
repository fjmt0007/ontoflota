/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.juegoTablero.MovimientoEntregado;

/**
 *
 * @author Javier Marin
 */
public interface TareasPintarTablero {
    public void pintarTablero(MovimientoEntregado movimiento);
    public MovimientoEntregado getMovimiento();
}
