/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.tareas;

import static es.uja.ssmmaa.ontologia.Constantes.TIME_OUT;
import es.uja.ssmmaa.ontologia.Vocabulario;
import es.uja.ssmmaa.ontologia.agentes.AgenteJugador;
import es.uja.ssmmaa.ontologia.agentes.AgenteTablero;
import es.uja.ssmmaa.ontologia.juegoTablero.EstadoPartida;
import es.uja.ssmmaa.ontologia.juegoTablero.MovimientoEntregado;
import es.uja.ssmmaa.ontologia.juegoTablero.PedirMovimiento;
import jade.content.ContentManager;
import jade.content.Predicate;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetResponder;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Marin
 */
public class TareaPedirMovimientoParticipante extends ContractNetResponder {

    private final AgenteJugador agente;
    private final ContentManager manager;

    public TareaPedirMovimientoParticipante(Agent a, MessageTemplate mt, ContentManager manager) {
        super(a, mt);
        agente = (AgenteJugador) a;
        this.manager = manager;

    }

    @Override
    protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose, ACLMessage accept) throws FailureException {
        try {
            MovimientoEntregado me = (MovimientoEntregado) manager.extractContent(accept);
            agente.aplicarMovimiento(me);
            ACLMessage msg = agente.estadoPartida(me.getPartida(), accept);

            //System.out.println("Enviando inform: " + msg);
            return msg;
        } catch (Codec.CodecException ex) {
            Logger.getLogger(TareaPedirMovimientoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(TareaPedirMovimientoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    protected ACLMessage handleCfp(ACLMessage cfp) throws RefuseException, FailureException, NotUnderstoodException {
        ACLMessage msg = null;
        try {
            //Obtenemos el mensaje
            Action accion = (Action) manager.extractContent(cfp);
            PedirMovimiento pm = (PedirMovimiento) accion.getAction();
            if (pm.getJugadorActivo().getAgenteJugador().compareTo(agente.getAID()) == 0) {
                msg = agente.mover(pm.getPartida(), pm.getJugadorActivo());
                Thread.sleep(300);
            } else {
                msg = agente.estadoPartida(pm.getPartida(), cfp);
            }
            msg.addReceiver(cfp.getSender());

        } catch (Codec.CodecException ex) {
            Logger.getLogger(TareaPedirMovimientoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(TareaPedirMovimientoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(TareaPedirMovimientoParticipante.class.getName()).log(Level.SEVERE, null, ex);
        }

        return msg;
    }

}
