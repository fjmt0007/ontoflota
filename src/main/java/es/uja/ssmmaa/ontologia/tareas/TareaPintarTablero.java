/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.tareas;

import es.uja.ssmmaa.ontologia.juegoTablero.MovimientoEntregado;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;

/**
 *
 * @author Javier Marin
 */
public class TareaPintarTablero extends CyclicBehaviour{

    TareasPintarTablero agente;
    public TareaPintarTablero(Agent a) {
        super(a);
        agente = (TareasPintarTablero) a;
    }

    @Override
    public void action() {
        MovimientoEntregado movimiento = agente.getMovimiento();
        if(movimiento!=null){
            agente.pintarTablero(movimiento);
        }
    }
    
}
