/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia;

import java.awt.Color;
import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador de números aleatorios
    public final Random aleatorio = new Random();
    
    // Constantes
    public static final long TIME_OUT = 4000; // 4segundos
    public static final int FINALIZACION = 3; // segundos
    public static final String PREFIJO_ID = "Dyy"; // Formato fecha para el idJuego
    public static final int VACIO = 0;
    public static final int[] MIN_JUGADORES = {2,4,4};
    public static final int[] MAX_JUGADORES = {2,16,8};
    public static final int PRIMERO = 0;
    public static final int MENOR = -1;
    public static final int IGUAL = 0;
    public static final int MAYOR = 1;
    public final int DIA = 1;
    public final int NUM_JUEGO = 2;
    public static final String DIRECTORIO = "./data/";
    public static final String ARCHIVO = "juegos.dat";
    public static final String DIVISOR = "-";
    
    public static int[] barcos = {4,3,3,2,2,2,1,1,1,1};
    
    public static int getNumBarcos(){
        int numBarcos=0;
        for (int i = 0; i < barcos.length; i++) {
            numBarcos+=barcos[i];
        }
        return numBarcos;
    }
    
    public static enum TipoCasilla {
        DESCONOCIDO(0), AGUA(1), BARCO(2), TORPEDO(3), TORPEDO_AGUA(4), TORPEDO_BARCO(5);

        private final int tipo;

        private TipoCasilla(int tipo) {
            this.tipo = tipo;
        }

        /**
         * Dimensión del tablero para el juego. Todos los juegos tienen un
         * tablero cuadrado.
         *
         * @return número de filas y columnas
         */
        public int getTipoCasilla() {
            return tipo;
        }

        public static TipoCasilla lanzarTorpedo(TipoCasilla c) {
            switch (c) {
                case AGUA:
                    return TORPEDO_AGUA;
                case BARCO:
                    return TORPEDO_BARCO;
                default:
                    return c;
            }
        }

        public static Color getColor(TipoCasilla c) {
            switch (c) {
                case DESCONOCIDO:
                    return Color.GRAY;
                case AGUA:
                    return Color.cyan;
                case BARCO:
                    return Color.BLACK;
                case TORPEDO_AGUA:
                    return Color.blue;
                case TORPEDO_BARCO:
                    return Color.RED;
                default:
                    return Color.PINK;
            }
        }
    }
    
    
}
