/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.hundirlaflota;

import es.uja.ssmmaa.ontologia.Vocabulario;
import es.uja.ssmmaa.ontologia.juegoTablero.InfoJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Tablero;

/**
 *
 * @author Javier Marin
 */
public class HundirLaFlota extends InfoJuego{
    private final Tablero tablero;

    public HundirLaFlota() {
        this.tablero = new Tablero(Vocabulario.TipoJuego.HUNDIR_LA_FLOTA.getDimension(),
                                   Vocabulario.TipoJuego.HUNDIR_LA_FLOTA.getDimension());
    }
    
    public int getFilas() {
        return tablero.getDimX();
    }
    
    public int getColumnas() {
        return tablero.getDimY();
    }

    @Override
    public String toString() {
        return "HundirLaFlota{" + "tablero=" + tablero + '}';
    }
}
