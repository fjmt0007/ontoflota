/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssmmaa.ontologia.hundirlaflota;

import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import jade.content.Predicate;
import jade.core.AID;
import java.util.ArrayList;

/**
 *
 * @author Javi
 */
public class BarcosColocados implements Predicate{
    ArrayList<Barco> barcos;
    String idPartida;
    Jugador jugador;

    public BarcosColocados() {
    }

    
    
    public BarcosColocados(ArrayList<Barco> barcos, String idPartida, Jugador jugador) {
        this.barcos = barcos;
        this.idPartida = idPartida;
        this.jugador = jugador;
    }

    public BarcosColocados(String idPartida, Jugador jugador) {
        this.idPartida = idPartida;
        this.jugador = jugador;
        this.barcos = new ArrayList<>();
    }
    
    public void addBarco(Barco barco){
        barcos.add(barco);
    }

    public ArrayList<Barco> getBarcos() {
        return barcos;
    }

    public String getIdPartida() {
        return idPartida;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setBarcos(ArrayList<Barco> barcos) {
        this.barcos = barcos;
    }

    public void setIdPartida(String idPartida) {
        this.idPartida = idPartida;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    @Override
    public String toString() {
        return "BarcosColocados{" + "barcos=" + barcos + ", idPartida=" + idPartida + ", jugador=" + jugador + '}';
    }
    
    
    
    
    
    
    
    
    
}
