/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssmmaa.ontologia.hundirlaflota;

import es.uja.ssmmaa.ontologia.juegoTablero.Posicion;
import jade.content.Concept;
import static java.lang.Math.abs;

/**
 *
 * @author Javi
 */
public class Barco implements Concept{
    Posicion inicio;
    Posicion fin;
    int casillas;
    int tocados;

    public Barco() {
    }

    
    public Barco(Posicion inicio, Posicion fin) {
        this.inicio = inicio;
        this.fin = fin;
        this.casillas = 1+ Integer.max(abs(inicio.getCoorX()-fin.getCoorX()), abs(inicio.getCoorY()-fin.getCoorY()));
        tocados=0;
    }
    
    public boolean isHundido(){
        return tocados==casillas;
    }

    public Posicion getInicio() {
        return inicio;
    }

    public Posicion getFin() {
        return fin;
    }

    public int getCasillas() {
        return casillas;
    }

    public int getTocados() {
        return tocados;
    }

    public void setInicio(Posicion inicio) {
        this.inicio = inicio;
    }

    public void setFin(Posicion fin) {
        this.fin = fin;
    }

    public void setCasillas(int casillas) {
        this.casillas = casillas;
    }

    public void setTocados(int tocados) {
        this.tocados = tocados;
    }

    @Override
    public String toString() {
        return "Barco{" + "inicio=" + inicio + ", fin=" + fin + ", casillas=" + casillas + ", tocados=" + tocados + '}';
    }
    
    
    
    
    
    
    
    
    
}
