/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.hundirlaflota;

import es.uja.ssmmaa.ontologia.Constantes;
import es.uja.ssmmaa.ontologia.Constantes.TipoCasilla;
import static es.uja.ssmmaa.ontologia.Constantes.TipoCasilla.AGUA;
import static es.uja.ssmmaa.ontologia.Constantes.TipoCasilla.BARCO;
import static es.uja.ssmmaa.ontologia.Constantes.TipoCasilla.DESCONOCIDO;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoJuego.HUNDIR_LA_FLOTA;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Movimiento;
import es.uja.ssmmaa.ontologia.juegoTablero.MovimientoEntregado;
import es.uja.ssmmaa.ontologia.juegoTablero.Partida;
import es.uja.ssmmaa.ontologia.juegoTablero.Partida;
import es.uja.ssmmaa.ontologia.juegoTablero.Posicion;
import es.uja.ssmmaa.ontologia.juegoTablero.Posicion;
import jade.content.Concept;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Javier Marin
 */
public class PartidaHundirLaFlota implements Concept {

    private String idPartida;
    private TipoCasilla[][] tableroJ1;
    private TipoCasilla[][] tableroJ2;
    private boolean j1Init = false;
    private boolean j2Init = false;
    private Jugador j1;
    private Jugador j2;
    private Partida partida;
    private int tamanio;
    private int numBarcos = 4;

    public PartidaHundirLaFlota() {
        idPartida = null;
        tableroJ1 = null;
        tableroJ2 = null;
        j1 = null;
        j2 = null;
        partida = null;
    }

    public PartidaHundirLaFlota(Jugador j1, Jugador j2, Partida partida, boolean tab) { //Si tab es true significa que es para agente tablero
        idPartida = partida.getIdPartida();

        this.j1 = j1;
        this.j2 = j2;
        this.partida = partida;
        this.tamanio = HUNDIR_LA_FLOTA.getDimension();
        //Inicializar tablero jugador 1
        this.tableroJ1 = new TipoCasilla[tamanio][tamanio];
        for (int i = 0; i < tableroJ1.length; i++) {
            for (int j = 0; j < tableroJ1[i].length; j++) {
                tableroJ1[i][j] = AGUA;
            }
        }
        //Inicializar tablero jugador 2
        TipoCasilla casillaJ2;
        if (tab) {
            casillaJ2 = AGUA;
        } else {
            casillaJ2 = DESCONOCIDO;
        }
        this.tableroJ2 = new TipoCasilla[tamanio][tamanio];
        for (int i = 0; i < tableroJ2.length; i++) {
            for (int j = 0; j < tableroJ2[i].length; j++) {
                tableroJ2[i][j] = casillaJ2;
            }
        }

    }

    public boolean partidaInicializada() {
        return j1Init && j2Init;
    }

//    public PartidaHundirLaFlota(PartidaHundirLaFlota p, int jugadorObjetivo) {
//        this.idPartida = p.idPartida;
//        this.j1 = p.j1;
//        this.j2 = p.j2;
//        this.partida = p.partida;
//        this.tamanio = p.tamanio;
//        if (jugadorObjetivo == 1) {
//            this.tableroJ1 = p.tableroJ1;
//            this.tableroJ2 = new TipoCasilla[tamanio][tamanio];
//            for (int i = 0; i < tableroJ2.length; i++) {
//                for (int j = 0; j < tableroJ2[i].length; j++) {
//                    tableroJ2[i][j] = DESCONOCIDO;
//                }
//            }
//        } else if (jugadorObjetivo == 2) {
//            this.tableroJ2 = p.tableroJ2;
//            this.tableroJ1 = new TipoCasilla[tamanio][tamanio];
//            for (int i = 0; i < tableroJ1.length; i++) {
//                for (int j = 0; j < tableroJ1[i].length; j++) {
//                    tableroJ1[i][j] = DESCONOCIDO;
//                }
//            }
//        }
//
//    }
    public void colocarBarcos(BarcosColocados barcosC) {
        TipoCasilla[][] tab = null;
        if (barcosC.getJugador().getAgenteJugador().compareTo(j1.getAgenteJugador()) == 0) {
            tab = tableroJ1;
            j1Init = true;
        } else if (barcosC.getJugador().getAgenteJugador().compareTo(j2.getAgenteJugador()) == 0) {
            tab = tableroJ2;
            j2Init = true;
        }
        ArrayList<Barco> barcos = barcosC.getBarcos();
        System.out.println("Procedo a colocar: " + barcos);
        for (int i = 0; i < barcos.size(); i++) {
            colocarBarcoMMM(barcos.get(i), tab);

        }
    }

    private void colocarBarcoMMM(Barco barco, TipoCasilla[][] tab) {
        int xMin = Integer.min(barco.getInicio().getCoorX(), barco.getFin().getCoorX());
        int xMax = Integer.max(barco.getInicio().getCoorX(), barco.getFin().getCoorX());

        int yMin = Integer.min(barco.getInicio().getCoorY(), barco.getFin().getCoorY());
        int yMax = Integer.max(barco.getInicio().getCoorY(), barco.getFin().getCoorY());

        for (int i = xMin; i <= xMax; i++) {
            for (int j = yMin; j <= yMax; j++) {
                tab[i][j] = BARCO;
            }
        }
    }

    public TipoCasilla lanzarTorpedo(Movimiento m) {
        int fila = m.getPosicion().getCoorX();
        int col = m.getPosicion().getCoorY();
        TipoCasilla[][] objetivo = null;
        Torpedo t = (Torpedo) m.getFicha();
        Jugador lanzador = t.getLanzador();
        if (lanzador.equals(j1)) {
            objetivo = tableroJ2;
        } else if (lanzador.equals(j2)) {
            objetivo = tableroJ1;
        } else {
            System.out.println("FALLO: NO EXISTE ESTE JUGADOR EN LA PARTIDA");
        }
        TipoCasilla actual = objetivo[fila][col];
        System.out.println("Pretorpedo: " + actual);
        actual = TipoCasilla.lanzarTorpedo(actual);
        System.out.println("Postorpedo: " + actual);
        return actual;

    }

    public TipoCasilla aplicarTorpedo(Movimiento m, TipoCasilla resultado) {
        int fila = m.getPosicion().getCoorX();
        int col = m.getPosicion().getCoorY();
        TipoCasilla[][] objetivo = null;
        Torpedo t = (Torpedo) m.getFicha();
        Jugador lanzador = t.getLanzador();
        if (lanzador.equals(j1)) {
            objetivo = tableroJ2;
        } else if (lanzador.equals(j2)) {
            objetivo = tableroJ1;
        } else {
            System.out.println("FALLO: NO EXISTE ESTE JUGADOR EN LA PARTIDA");
        }
        objetivo[fila][col] = resultado;
        return objetivo[fila][col];
    }

    public BarcosColocados inicializarTablero(PedirBarcos pb) {
        Jugador jugador = pb.getJugador();
        String idPartida = pb.getIdPartida();
        ArrayList<Integer> barcosTam = pb.getBarcos();
        ArrayList<Barco> barcos = new ArrayList<>();

        for (int i = 0; i < barcosTam.size(); i++) {
            Number n = barcosTam.get(i);

            Barco barco = colocarBarco(n.intValue(), tableroJ1);

            barcos.add(barco);

        }

        return new BarcosColocados(barcos, idPartida, jugador);
    }

    public void printT1() {
        for (int i = 0; i < tableroJ1.length; i++) {
            for (int j = 0; j < tableroJ1[i].length; j++) {
                System.out.print(tableroJ1[i][j]);

            }
            System.out.println("");

        }
    }

    public void printT2() {
        for (int i = 0; i < tableroJ2.length; i++) {
            for (int j = 0; j < tableroJ2[i].length; j++) {
                System.out.print(tableroJ2[i][j]);

            }
            System.out.println("");

        }
    }

    private boolean casillaAislada(int x, int y, TipoCasilla[][] tablero) {
        boolean aislada = true;
        if (x - 1 >= 0) {
            if (tablero[x - 1][y] != AGUA) {
                aislada = false;
            }
        }
        if (x + 1 < tamanio) {
            if (tablero[x + 1][y] != AGUA) {
                aislada = false;
            }
        }
        if (y - 1 >= 0) {
            if (tablero[x][y - 1] != AGUA) {
                aislada = false;
            }
        }
        if (y + 1 < tamanio) {
            if (tablero[x][y + 1] != AGUA) {
                aislada = false;
            }
        }
        return aislada;
    }

    private Barco colocarBarco(int num, TipoCasilla[][] tablero) {
        Random rand = Constantes.aleatorio;
        ArrayList<Posicion> descartadas = new ArrayList<>();
        boolean colocado = false;
        Posicion inicio = new Posicion();
        Posicion fin = new Posicion();
        while (!colocado) {
            inicio = new Posicion(rand.nextInt(tamanio - 1), rand.nextInt(tamanio - 1));
            if (!descartadas.contains(inicio) && tablero[inicio.getCoorX()][inicio.getCoorY()] == AGUA) {
                if (num == 1 && casillaAislada(inicio.getCoorX(), inicio.getCoorY(), tablero)) { //Es el barco de una casilla,miramos que no tenga barcos alrededor, la asignamos y punto
                    tablero[inicio.getCoorX()][inicio.getCoorY()] = BARCO;
                    colocado = true;
                    fin = new Posicion(inicio.getCoorX(), inicio.getCoorY());
                } else {
                    ArrayList<String> direccion = new ArrayList<>();
                    boolean dir = true;

                    //Vemos si la derecha es una opcion
                    if (inicio.getCoorX() + (num - 1) < tamanio) {
                        for (int i = 0; i < num; i++) {
                            if (!casillaAislada(inicio.getCoorX() + i, inicio.getCoorY(), tablero)) {//Revisa la casilla objetivo y las contiguas
                                dir = false;//Esta direccion ya no vale
                            }
                        }
                        if (dir) {
                            direccion.add("Derecha");
                        }
                    }

                    //Vemos si la izquierda es una opcion
                    if (inicio.getCoorX() - (num - 1) >= 0) {
                        for (int i = 0; i < num; i++) {
                            if (!casillaAislada(inicio.getCoorX() - i, inicio.getCoorY(), tablero)) {
                                dir = false;//Esta direccion ya no vale
                            }
                        }
                        if (dir) {
                            direccion.add("Izquierda");
                        }
                    }

                    //Vemos si arriba es una opcion
                    if (inicio.getCoorY() - (num - 1) >= 0) {
                        for (int i = 0; i < num; i++) {
                            if (!casillaAislada(inicio.getCoorX(), inicio.getCoorY() - i, tablero)) {
                                dir = false;//Esta direccion ya no vale
                            }
                        }
                        if (dir) {
                            direccion.add("Arriba");
                        }
                    }

                    //Vemos si abajo es una opcion
                    if (inicio.getCoorY() + (num - 1) < tamanio) {
                        for (int i = 0; i < num; i++) {
                            if (!casillaAislada(inicio.getCoorX(), inicio.getCoorY() + i, tablero)) {
                                dir = false;//Esta direccion ya no vale
                            }
                        }
                        if (dir) {
                            direccion.add("Abajo");
                        }
                    }

                    if (direccion.size() > 1) {
                        String direccionEscogida = direccion.get(rand.nextInt(direccion.size() - 1));

                        switch (direccionEscogida) {
                            case "Derecha":
                                for (int i = 0; i < num; i++) {
                                    tablero[inicio.getCoorX() + i][inicio.getCoorY()] = BARCO;
                                }
                                colocado = true;
                                fin = new Posicion(inicio.getCoorX() + (num - 1), inicio.getCoorY());
                                break;

                            case "Izquierda":
                                for (int i = 0; i < num; i++) {
                                    tablero[inicio.getCoorX() - i][inicio.getCoorY()] = BARCO;
                                }
                                colocado = true;
                                fin = new Posicion(inicio.getCoorX() - (num - 1), inicio.getCoorY());
                                break;

                            case "Arriba":
                                for (int i = 0; i < num; i++) {
                                    tablero[inicio.getCoorX()][inicio.getCoorY() - i] = BARCO;
                                }
                                colocado = true;
                                fin = new Posicion(inicio.getCoorX(), inicio.getCoorY() - (num - 1));
                                break;

                            case "Abajo":
                                for (int i = 0; i < num; i++) {
                                    tablero[inicio.getCoorX()][inicio.getCoorY() + i] = BARCO;
                                }
                                colocado = true;
                                fin = new Posicion(inicio.getCoorX(), inicio.getCoorY() + (num - 1));
                                break;
                        }
                    }//Fin: no hay direcciones disponibles
                }
            } else {
                descartadas.add(inicio);
            }
        }
        return new Barco(inicio, fin);
    }

    public String getIdPartida() {
        return idPartida;
    }

    public void setIdPartida(String idPartida) {
        this.idPartida = idPartida;
    }

    public Jugador getJ1() {
        return j1;
    }

    public void setJ1(Jugador j1) {
        this.j1 = j1;
    }

    public Jugador getJ2() {
        return j2;
    }

    public void setJ2(Jugador j2) {
        this.j2 = j2;
    }

    public Partida getPartida() {
        return partida;
    }

    public void setPartida(Partida partida) {
        this.partida = partida;
    }

    @Override
    public String toString() {
        return "PartidaHundirLaFlota{" + "idPertida=" + idPartida + ", tableroJ1=" + tableroJ1 + ", tableroJ2=" + tableroJ2 + ", j1=" + j1 + ", j2=" + j2 + ", partida=" + partida + ", tamanio=" + tamanio + '}';
    }

    public TipoCasilla[][] getTableroJ1() {
        return this.tableroJ1;
    }

    public TipoCasilla[][] getTableroJ2() {
        return this.tableroJ2;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    public int getNumBarcos() {
        return numBarcos;
    }

    public void setNumBarcos(int numBarcos) {
        this.numBarcos = numBarcos;
    }

}
