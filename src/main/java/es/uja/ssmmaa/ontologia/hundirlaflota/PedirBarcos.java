/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssmmaa.ontologia.hundirlaflota;

import es.uja.ssmmaa.ontologia.Constantes;
import es.uja.ssmmaa.ontologia.Vocabulario;
import es.uja.ssmmaa.ontologia.juegoTablero.FichaJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Partida;
import es.uja.ssmmaa.ontologia.juegoTablero.Partida;
import jade.content.AgentAction;
import jade.content.Concept;
import jade.content.onto.annotations.Slot;
import jade.util.leap.List;
import java.util.ArrayList;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;

/**
 *
 * @author Javi
 */
public class PedirBarcos implements AgentAction{
    private ArrayList<Integer> barcos;
    private String idPartida;
    private Partida partida;
    private int tamTablero;
    private Jugador jugador;
    private Jugador rival;

    public PedirBarcos() {
        this.idPartida = "";
        this.partida = null;
        this.tamTablero=0;
        this.jugador=null;
    }

    

    

    public PedirBarcos(Partida partida, Jugador jugador, Jugador rival) {
        this.idPartida = partida.getIdPartida();
        this.partida=partida;
        this.jugador=jugador;
        this.rival = rival;
        this.barcos = new ArrayList<Integer>();
        inicializarBarcos();
        tamTablero = Vocabulario.TipoJuego.HUNDIR_LA_FLOTA.getDimension();
        
    }

    public Jugador getRival() {
        return rival;
    }

    public void setRival(Jugador rival) {
        this.rival = rival;
    }

    public Jugador getJugador() {
        return jugador;
    }
    
    
    private void inicializarBarcos(){
        int[] cBarcos = Constantes.barcos;
        for (int i = 0; i < cBarcos.length; i++) {
            barcos.add(cBarcos[i]);
        }
    }

    public ArrayList<Integer> getBarcos() {
        return barcos;
    }

    @Slot(mandatory=true)
    public String getIdPartida() {
        return idPartida;
    }


    public int getTamTablero() {
        return tamTablero;
    }

    public Partida getPartida() {
        return partida;
    }

    public void setIdPartida(String idPartida) {
        this.idPartida = idPartida;
    }

    public void setPartida(Partida partida) {
        this.partida = partida;
    }

    public void setTamTablero(int tamTablero) {
        this.tamTablero = tamTablero;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    @Override
    public String toString() {
        return "PedirBarcos{" + "barcos=" + barcos + ", idPartida=" + idPartida + ", partida=" + partida + ", tamTablero=" + tamTablero + ", jugador=" + jugador + '}';
    }

    public void setBarcos(ArrayList<Integer> barcos) {
        this.barcos = barcos;
    }
    
    
    
}
