/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.hundirlaflota;

import es.uja.ssmmaa.ontologia.Constantes;
import es.uja.ssmmaa.ontologia.Constantes.TipoCasilla;
import es.uja.ssmmaa.ontologia.Vocabulario;
import es.uja.ssmmaa.ontologia.juegoTablero.FichaJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import jade.content.onto.annotations.Slot;

/**
 *
 * @author Javier Marin
 */
public class Torpedo extends FichaJuego {
    private Jugador lanzador;
    private TipoCasilla tipoCasilla;

    public Torpedo() {
        this.lanzador = null;
    }

    public Torpedo(Jugador jugador, TipoCasilla tc) {
        this.lanzador = jugador;
        this.tipoCasilla = tc;
    }

    @Slot(mandatory=true)
    public Jugador getLanzador() {
        return lanzador;
    }

    public void setLanzador(Jugador jugador) {
        this.lanzador = jugador;
    }

    public TipoCasilla getTipoCasilla() {
        return tipoCasilla;
    }

    public void setTipoCasilla(TipoCasilla tipoCasilla) {
        this.tipoCasilla = tipoCasilla;
    }

    @Override
    public String toString() {
        return "Ficha{" + "jugador=" + lanzador + ", tipoCasilla=" + tipoCasilla + '}';
    }

    
}
