/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.hundirlaflota;

import jade.core.AID;

/**
 *
 * @author Javier Marin
 */
public class Peticion {
    String idPartida;
    AID jugadorActivo;

    public Peticion(String idPartida, AID jugadorActivo) {
        this.idPartida = idPartida;
        this.jugadorActivo = jugadorActivo;
    }

    public String getIdPartida() {
        return idPartida;
    }

    public void setIdPartida(String idPartida) {
        this.idPartida = idPartida;
    }

    public AID getJugadorActivo() {
        return jugadorActivo;
    }

    public void setJugadorActivo(AID jugadorActivo) {
        this.jugadorActivo = jugadorActivo;
    }

    @Override
    public String toString() {
        return "Peticion{" + "idPartida=" + idPartida + ", jugadorActivo=" + jugadorActivo + '}';
    }
    
}
