/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.gui;


import static es.uja.ssmmaa.ontologia.Constantes.VACIO;
import static es.uja.ssmmaa.ontologia.Vocabulario.JUEGOS;
import es.uja.ssmmaa.ontologia.Vocabulario.Modo;
import es.uja.ssmmaa.ontologia.Vocabulario.TipoJuego;
import es.uja.ssmmaa.ontologia.agentes.AgenteMonitor;
import jade.core.AID;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JRadioButton;

/**
 *
 * @author pedroj
 */
public class MonitorJFrame extends javax.swing.JFrame {
    private final AgenteMonitor agente;
    private OkCancelDialog finalizacion;
    private List<AID>[] agentesOrganizador;
    private List<String>[][] juegosIncompletos;
    private List<String>[][] juegosDisponibles;

    /**
     * Creates new form Consola
     * @param agente
     */
    public MonitorJFrame(AgenteMonitor agente) {
        super("Juegos Tablero " + agente.getName());
        initComponents();
        
        this.agente = agente;
        
        // Inicializamos la lista de agentes organizador
        int modos = Modo.values().length;
        agentesOrganizador = new List[JUEGOS.length];
        juegosIncompletos = new List[JUEGOS.length][modos];
        juegosDisponibles = new List[JUEGOS.length][modos];
        for(int i = 0; i < JUEGOS.length; i++) {
            agentesOrganizador[i] = new LinkedList();
            for(int j = 0; j < modos; j++) {
                juegosIncompletos[i][j] = new LinkedList();
                juegosDisponibles[i][j] = new LinkedList();
            }
        }
        
        // Completamos los modos de tipoJuego
        jComboBoxModo.removeAllItems();
        for( Modo modo : Modo.values() ) {
            jComboBoxModo.addItem(modo.name());
        }
        
        setVisible(true);
    }
    
    public boolean hayJugadores(TipoJuego tipoJuego) {
        Modo tipoModo = getModoJuego();
        
        return agente.hayJugadores(tipoJuego, tipoModo);
    }
    
    public void addOrganizador(AID agente, TipoJuego tipoJuego) {
        agentesOrganizador[tipoJuego.ordinal()].add(agente);
        mostrarOrganizadores(getTipoJuego());
    }
    
    public void removeOrganizador(AID agente, TipoJuego tipoJuego) {
        agentesOrganizador[tipoJuego.ordinal()].remove(agente);
        mostrarOrganizadores(getTipoJuego());
    }
    
    public void addJuegoIncompleto(String idJuego, TipoJuego tipoJuego, Modo modo) {
        juegosIncompletos[tipoJuego.ordinal()][modo.ordinal()].add(idJuego);
        mostrarJuegosIncompletos(getTipoJuego(), modo);
    }
    
    public void removeJuegoIncompleto(String idJuego, TipoJuego tipoJuego, Modo modo) {
        juegosIncompletos[tipoJuego.ordinal()][modo.ordinal()].remove(idJuego);
        mostrarJuegosIncompletos(getTipoJuego(),modo);
    }
    
    public void addJuegoDisponible(String idJuego, TipoJuego tipoJuego, Modo modo) {
        juegosDisponibles[tipoJuego.ordinal()][modo.ordinal()].add(idJuego);
        mostrarJuegosDisponibles(getTipoJuego(),modo);
    }
    
    public void removeJuegoDisponible(String idJuego, TipoJuego tipoJuego, Modo modo) {
        juegosDisponibles[tipoJuego.ordinal()][modo.ordinal()].remove(idJuego);
        mostrarJuegosDisponibles(getTipoJuego(),modo);
    }
    
    public void proponerJuego(TipoJuego tipoJuego) {
        TipoJuego seleccionado = getTipoJuego();
        
        if( seleccionado.equals(tipoJuego) )
            proponerJuegoJButton.setEnabled(hayJugadores(tipoJuego));
    }
    
    private void mostrarJuegosIncompletos(TipoJuego tipoJuego, Modo modo) {
        boolean activar;
        
        jComboBoxJuegosIncompletos.removeAllItems();
        for( String juego : juegosIncompletos[tipoJuego.ordinal()][modo.ordinal()] ) {
            jComboBoxJuegosIncompletos.addItem(juego);
        }
        
        activar = jComboBoxJuegosIncompletos.getItemCount() > VACIO;
        completarJuegoJButton.setEnabled(activar);
    }
    
    private void mostrarOrganizadores(TipoJuego tipoJuego) {
        boolean activar;
        
        jComboBoxAgentesOrganizador.removeAllItems();
        for( AID agente : agentesOrganizador[tipoJuego.ordinal()] ) {
                jComboBoxAgentesOrganizador.addItem(agente.getLocalName());
        }
       
        activar = (jComboBoxAgentesOrganizador.getItemCount() > VACIO) &&
                  (jComboBoxJuegosDisponibles.getItemCount() > VACIO);
        finalizarJuegoJButton.setEnabled(activar);
    }
    
    private void mostrarJuegosDisponibles(TipoJuego tipoJuego, Modo modo) {
        boolean activar;
        
        jComboBoxJuegosDisponibles.removeAllItems();
        for( String juego : juegosDisponibles[tipoJuego.ordinal()][modo.ordinal()] ) {
            jComboBoxJuegosDisponibles.addItem(juego);
        }
        
        activar = (jComboBoxAgentesOrganizador.getItemCount() > VACIO) &&
                  (jComboBoxJuegosDisponibles.getItemCount() > VACIO);
        finalizarJuegoJButton.setEnabled(activar);
    }
    
    private Modo getModoJuego() {
        Modo tipoModo = null;
        
        for ( Modo modo : Modo.values() )
            if ( jComboBoxModo.getSelectedItem().toString().equals(modo.name()) ) {
                tipoModo = modo;
                break;
            }
        
        return tipoModo;
    }
    
    private TipoJuego getTipoJuego() {
        JRadioButton botonJuego = null;
        Enumeration elm = grupoJuegos.getElements();
       
        // Buscamos el botón seleccionado
        while( elm.hasMoreElements() ) {
            botonJuego = (JRadioButton) elm.nextElement();
            if( botonJuego.getModel() == grupoJuegos.getSelection() ) {
                break;
            }
        }        
                
       return TipoJuego.valueOf(botonJuego.getText());
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoJuegos = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxAgentesOrganizador = new javax.swing.JComboBox<>();
        jComboBoxJuegosDisponibles = new javax.swing.JComboBox<>();
        finalizarJuegoJButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxModo = new javax.swing.JComboBox<>();
        proponerJuegoJButton = new javax.swing.JButton();
        juegoQuoridor = new javax.swing.JRadioButton();
        juegoQuatro = new javax.swing.JRadioButton();
        juegoTresEnRaya = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jComboBoxJuegosIncompletos = new javax.swing.JComboBox<>();
        completarJuegoJButton = new javax.swing.JButton();
        juegoHundirLaFlota = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setText("Agentes ORGANIZADOR");

        jComboBoxAgentesOrganizador.setMaximumSize(new java.awt.Dimension(40, 40));

        finalizarJuegoJButton.setText(" Finalizar Juego");
        finalizarJuegoJButton.setEnabled(false);
        finalizarJuegoJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizarJuegoJButtonActionPerformed(evt);
            }
        });

        jLabel6.setText("Juegos Disponibles");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(finalizarJuegoJButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jComboBoxAgentesOrganizador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(jComboBoxJuegosDisponibles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 216, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxAgentesOrganizador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxJuegosDisponibles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
                .addComponent(finalizarJuegoJButton)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Tipo Juego");

        jLabel2.setText("Modo Juego");

        jComboBoxModo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxModoActionPerformed(evt);
            }
        });

        proponerJuegoJButton.setText("Proponer Juego");
        proponerJuegoJButton.setEnabled(false);
        proponerJuegoJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proponerJuegoJButtonActionPerformed(evt);
            }
        });

        grupoJuegos.add(juegoQuoridor);
        juegoQuoridor.setText("QUORIDOR");
        juegoQuoridor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                juegoQuoridorActionPerformed(evt);
            }
        });

        grupoJuegos.add(juegoQuatro);
        juegoQuatro.setText("QUATRO");
        juegoQuatro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                juegoQuatroActionPerformed(evt);
            }
        });

        grupoJuegos.add(juegoTresEnRaya);
        juegoTresEnRaya.setText("TRES_EN_RAYA");
        juegoTresEnRaya.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                juegoTresEnRayaActionPerformed(evt);
            }
        });

        jLabel3.setText("Juegos Incompletos");

        completarJuegoJButton.setText("Completar Juego");
        completarJuegoJButton.setEnabled(false);
        completarJuegoJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                completarJuegoJButtonActionPerformed(evt);
            }
        });

        grupoJuegos.add(juegoHundirLaFlota);
        juegoHundirLaFlota.setText("HUNDIR_LA_FLOTA");
        juegoHundirLaFlota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                juegoHundirLaFlotaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(completarJuegoJButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(proponerJuegoJButton))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jComboBoxModo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1)
                                .addGap(46, 46, 46))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(106, 106, 106)
                                .addComponent(juegoTresEnRaya)
                                .addGap(0, 23, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jComboBoxJuegosIncompletos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(juegoQuatro)
                            .addComponent(juegoHundirLaFlota)
                            .addComponent(juegoQuoridor))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxModo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(juegoTresEnRaya)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(juegoQuoridor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(juegoQuatro)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(juegoHundirLaFlota)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxJuegosIncompletos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(proponerJuegoJButton)
                    .addComponent(completarJuegoJButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        finalizacion = new OkCancelDialog(this, true, agente);
        finalizacion.setVisible(true);
    }//GEN-LAST:event_formWindowClosing

    private void finalizarJuegoJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finalizarJuegoJButtonActionPerformed
        // TODO add your handling code here:
        TipoJuego tipoJuego = getTipoJuego();
        int indice = jComboBoxAgentesOrganizador.getSelectedIndex();
        String idJuego = (String) jComboBoxJuegosDisponibles.getSelectedItem();
        agente.finalizarJuego(idJuego, agentesOrganizador[tipoJuego.ordinal()].get(indice));
    }//GEN-LAST:event_finalizarJuegoJButtonActionPerformed

    private void juegoHundirLaFlotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_juegoHundirLaFlotaActionPerformed
        // TODO add your handling code here:
        Modo modo = Modo.valueOf((String)jComboBoxModo.getSelectedItem());
        TipoJuego tipoJuego = getTipoJuego();
        mostrarOrganizadores(tipoJuego);
        mostrarJuegosIncompletos(tipoJuego,modo);
        mostrarJuegosDisponibles(tipoJuego,modo);
        proponerJuegoJButton.setEnabled(hayJugadores(tipoJuego));
    }//GEN-LAST:event_juegoHundirLaFlotaActionPerformed

    private void completarJuegoJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_completarJuegoJButtonActionPerformed
        // TODO add your handling code here:
        Modo modo = Modo.valueOf((String)jComboBoxModo.getSelectedItem());
        TipoJuego tipoJuego = getTipoJuego();
        String idJuego = (String) jComboBoxJuegosIncompletos.getSelectedItem();
        removeJuegoIncompleto(idJuego, tipoJuego, modo);
        agente.completarJuego(idJuego, tipoJuego);
    }//GEN-LAST:event_completarJuegoJButtonActionPerformed

    private void juegoTresEnRayaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_juegoTresEnRayaActionPerformed
        // TODO add your handling code here:
        Modo modo = Modo.valueOf((String)jComboBoxModo.getSelectedItem());
        TipoJuego tipoJuego = getTipoJuego();
        mostrarOrganizadores(tipoJuego);
        mostrarJuegosIncompletos(tipoJuego,modo);
        mostrarJuegosDisponibles(tipoJuego,modo);
        proponerJuegoJButton.setEnabled(hayJugadores(tipoJuego));
    }//GEN-LAST:event_juegoTresEnRayaActionPerformed

    private void juegoQuatroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_juegoQuatroActionPerformed
        // TODO add your handling code here:
        Modo modo = Modo.valueOf((String)jComboBoxModo.getSelectedItem());
        TipoJuego tipoJuego = getTipoJuego();
        mostrarOrganizadores(tipoJuego);
        mostrarJuegosIncompletos(tipoJuego,modo);
        mostrarJuegosDisponibles(tipoJuego,modo);
        proponerJuegoJButton.setEnabled(hayJugadores(tipoJuego));
    }//GEN-LAST:event_juegoQuatroActionPerformed

    private void juegoQuoridorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_juegoQuoridorActionPerformed
        // TODO add your handling code here:
        Modo modo = Modo.valueOf((String)jComboBoxModo.getSelectedItem());
        TipoJuego tipoJuego = getTipoJuego();
        mostrarOrganizadores(tipoJuego);
        mostrarJuegosIncompletos(tipoJuego,modo);
        mostrarJuegosDisponibles(tipoJuego,modo);
        proponerJuegoJButton.setEnabled(hayJugadores(tipoJuego));
    }//GEN-LAST:event_juegoQuoridorActionPerformed

    private void proponerJuegoJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proponerJuegoJButtonActionPerformed
        // TODO add your handling code here:
        // Proponemos un tipoJuego con los parámetros seleccionados
        Modo modoJuego = getModoJuego();
        TipoJuego tipoJuego = getTipoJuego();

        agente.proponerJuego(tipoJuego, modoJuego);
    }//GEN-LAST:event_proponerJuegoJButtonActionPerformed

    private void jComboBoxModoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxModoActionPerformed
        // TODO add your handling code here:
        Modo modo = Modo.valueOf((String)jComboBoxModo.getSelectedItem());
        TipoJuego tipoJuego = getTipoJuego();
        mostrarOrganizadores(tipoJuego);
        mostrarJuegosIncompletos(tipoJuego,modo);
        mostrarJuegosDisponibles(tipoJuego,modo);
        proponerJuegoJButton.setEnabled(hayJugadores(tipoJuego));
    }//GEN-LAST:event_jComboBoxModoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton completarJuegoJButton;
    private javax.swing.JButton finalizarJuegoJButton;
    private javax.swing.ButtonGroup grupoJuegos;
    private javax.swing.JComboBox<String> jComboBoxAgentesOrganizador;
    private javax.swing.JComboBox<String> jComboBoxJuegosDisponibles;
    private javax.swing.JComboBox<String> jComboBoxJuegosIncompletos;
    private javax.swing.JComboBox<String> jComboBoxModo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton juegoHundirLaFlota;
    private javax.swing.JRadioButton juegoQuatro;
    private javax.swing.JRadioButton juegoQuoridor;
    private javax.swing.JRadioButton juegoTresEnRaya;
    private javax.swing.JButton proponerJuegoJButton;
    // End of variables declaration//GEN-END:variables

}
