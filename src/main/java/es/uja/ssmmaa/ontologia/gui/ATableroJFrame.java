/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.gui;

import es.uja.ssmmaa.ontologia.Constantes.TipoCasilla;
import es.uja.ssmmaa.ontologia.hundirlaflota.PartidaHundirLaFlota;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Posicion;
import jade.core.Agent;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;

/**
 *
 * @author Javier Marin
 */
public class ATableroJFrame extends javax.swing.JFrame {

    private JButton b1[][];
    private JButton b2[][];

    private PartidaHundirLaFlota partida;
    private final Agent agente;
    private int t;

    /**
     * Creates new form Tablero2JFrame
     */
    public ATableroJFrame(Agent agente, PartidaHundirLaFlota partida) {
        super(agente.getLocalName() + "-" + partida.getIdPartida());
        this.agente = agente;
        this.partida = partida;
        initComponents();
        setVisible(true);
        int numCasillas = partida.getTableroJ1().length;
        t = partida.getTableroJ1().length;

        //Jugador1
        j1tab.setLayout(new GridLayout(t, t));
        b1 = new JButton[t][t];
        for (int i = 0; i < b1.length; i++) {
            for (int j = 0; j < b1.length; j++) {
                b1[i][j] = new JButton();
                b1[i][j].setBackground(TipoCasilla.getColor(partida.getTableroJ1()[i][j]));
                b1[i][j].setEnabled(false);
                b1[i][j].setPreferredSize(new Dimension(20, 20));
                j1tab.add(b1[i][j]);

            }
        }
        //Jugador2
        j2tab.setLayout(new GridLayout(t, t));
        b2 = new JButton[t][t];
        for (int i = 0; i < b2.length; i++) {
            for (int j = 0; j < b2.length; j++) {
                b2[i][j] = new JButton();
                b2[i][j].setBackground(TipoCasilla.getColor(partida.getTableroJ2()[i][j]));
                b2[i][j].setEnabled(false);
                b2[i][j].setPreferredSize(new Dimension(20, 20));
                j2tab.add(b2[i][j]);

            }
        }

        jLabel1.setText(partida.getJ1().getNombre());
        if (partida.getJ2() != null) {
            jLabel2.setText(partida.getJ2().getNombre());
        }

    }

    public void torpedoLanzado(Jugador lanzador, Posicion p, TipoCasilla resultado) {
        int col = p.getCoorX();
        int fila = p.getCoorY();

        int objetivo = 0;

        if (this.partida.getJ1().getAgenteJugador().compareTo(lanzador.getAgenteJugador()) == 0) {
            objetivo = 2;
        } else if (this.partida.getJ2().getAgenteJugador().compareTo(lanzador.getAgenteJugador()) == 0) {
            objetivo = 1;
        }
        switch (objetivo) {
            case 1:
                //Asignamos la nueva casilla
                //partida.getTableroJ1()[col][fila] = TipoCasilla.lanzarTorpedo(partida.getTableroJ1()[col][fila]);
                b1[col][fila].setBackground(TipoCasilla.getColor(resultado));
                b1[col][fila].setVisible(false);
                b1[col][fila].setVisible(true);
                //this.setVisible(false);
                //this.setVisible(true);
                break;
            case 2:
                //Asignamos la nueva casilla
                //partida.getTableroJ2()[col][fila] = TipoCasilla.lanzarTorpedo(partida.getTableroJ2()[col][fila]);
                b2[col][fila].setBackground(TipoCasilla.getColor(resultado));
                b2[col][fila].setVisible(false);
                b2[col][fila].setVisible(true);
                //this.setVisible(false);
                //this.setVisible(true);
                break;
        }
    }

    public TipoCasilla lanzarTorpedo(Jugador lanzador, Posicion p) {
        int col = p.getCoorX();
        int fila = p.getCoorY();

        int objetivo = 0;

        if (this.partida.getJ1().getAgenteJugador().compareTo(lanzador.getAgenteJugador()) == 0) {
            objetivo = 2;
        } else if (this.partida.getJ2().getAgenteJugador().compareTo(lanzador.getAgenteJugador()) == 0) {
            objetivo = 1;
        }

        //System.out.println(agente.getLocalName() + ": Aplicando movimiento " + p + " al objetivo " + objetivo);
        switch (objetivo) {
            case 1:
                //Asignamos la nueva casilla
                partida.getTableroJ1()[col][fila] = TipoCasilla.lanzarTorpedo(partida.getTableroJ1()[col][fila]);
                b1[col][fila].setBackground(TipoCasilla.getColor(partida.getTableroJ1()[col][fila]));
                b1[col][fila].setVisible(false);
                b1[col][fila].setVisible(true);
                //this.setVisible(false);
                //this.setVisible(true);
                return partida.getTableroJ1()[col][fila];
            case 2:
                //Asignamos la nueva casilla
                partida.getTableroJ2()[col][fila] = TipoCasilla.lanzarTorpedo(partida.getTableroJ2()[col][fila]);
                b2[col][fila].setBackground(TipoCasilla.getColor(partida.getTableroJ2()[col][fila]));
                b2[col][fila].setVisible(false);
                b2[col][fila].setVisible(true);
                //this.setVisible(false);
                //this.setVisible(true);
                return partida.getTableroJ2()[col][fila];
        }
        return null;

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        j1tab = new javax.swing.JPanel();
        j2tab = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        j1tab.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout j1tabLayout = new javax.swing.GroupLayout(j1tab);
        j1tab.setLayout(j1tabLayout);
        j1tabLayout.setHorizontalGroup(
            j1tabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        j1tabLayout.setVerticalGroup(
            j1tabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        j2tab.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout j2tabLayout = new javax.swing.GroupLayout(j2tab);
        j2tab.setLayout(j2tabLayout);
        j2tabLayout.setHorizontalGroup(
            j2tabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        j2tabLayout.setVerticalGroup(
            j2tabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel1.setText("Jugador1");

        jLabel2.setText("Jugador2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(123, 123, 123))
            .addGroup(layout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addComponent(j1tab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 229, Short.MAX_VALUE)
                .addComponent(j2tab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(95, 95, 95))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(166, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(j2tab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(j1tab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(79, 79, 79))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel j1tab;
    private javax.swing.JPanel j2tab;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
