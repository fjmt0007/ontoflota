/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.juegoTablero;

import jade.content.onto.annotations.Slot;
import jade.core.AID;
import java.util.Objects;

/**
 *
 * @author pedroj
 */
public class Jugador extends AgenteJuego {
    private String nombre;
    private AID agenteJugador;

    public Jugador() {
        this.nombre = null;
        this.agenteJugador = null;
    }

    public Jugador(String nombre, AID agenteJugador) {
        this.nombre = nombre;
        this.agenteJugador = agenteJugador;
    }

    @Slot(mandatory=true)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Slot(mandatory=true)
    public AID getAgenteJugador() {
        return agenteJugador;
    }

    public void setAgenteJugador(AID agenteJugador) {
        this.agenteJugador = agenteJugador;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jugador other = (Jugador) obj;
        
        if (this.agenteJugador.compareTo(other.getAgenteJugador())!=0) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return "Jugador{" + "nombre=" + nombre + '}';
    }
}
