/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.util;

import static es.uja.ssmmaa.ontologia.Constantes.MAX_JUGADORES;
import static es.uja.ssmmaa.ontologia.Constantes.MIN_JUGADORES;
import es.uja.ssmmaa.ontologia.Vocabulario.Modo;
import static es.uja.ssmmaa.ontologia.Vocabulario.Modo.ELIMINATORIA;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import jade.core.AID;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedroj
 */
public class RegistroJuego {
    private final Juego juego;
    private final Modo modoJuego;
    private final List<Jugador> jugadores;

    public RegistroJuego(Juego juego, Modo modoJuego) {
        this.juego = juego;
        this.modoJuego = modoJuego;
        this.jugadores = new ArrayList();
    }

    public Juego getJuego() {
        return juego;
    }

    public Modo getModoJuego() {
        return modoJuego;
    }

    public List<Jugador> getJugadores() {
        return jugadores;
    }
    
    public Jugador getJugador(AID agenteJugador) {
        for(Jugador jugador : jugadores) 
            if( jugador.getAgenteJugador().equals(agenteJugador) )
                return jugador;
        
        return null;
    }

    public int numJugadores() {
        return jugadores.size();
    }
    
    public void addJugador(Jugador jugador) {
        jugadores.add(jugador);
    }
    
    public boolean removeJugador(Jugador jugador) {
        return jugadores.remove(jugador);
    }
    
    public boolean removeJugador(AID agenteJugador) {
        for(Jugador jugador : jugadores) 
            if( jugador.getAgenteJugador().equals(agenteJugador) )
                return jugadores.remove(jugador);
        
        return false;
    }
    
    public boolean juadorRegistrado(AID agenteJugador) {
        return jugadores.contains(agenteJugador);
    }
    
    public boolean registroCompleto() {
        int numJug = jugadores.size();
        
        if( modoJuego.equals(ELIMINATORIA) )
            // Tiene que ser potencia de dos 
            return ((numJug & (numJug - 1)) == 0) && 
                   (numJug >= MIN_JUGADORES[ELIMINATORIA.ordinal()]);
        
        return numJug >= MIN_JUGADORES[modoJuego.ordinal()];
    }
    
    public int huecosLibres() {
        return MAX_JUGADORES[modoJuego.ordinal()] - jugadores.size();
    }

    @Override
    public String toString() {
        return "RegistroJuego{" + "juego=" + juego + ", modoJuego=" + modoJuego + 
                ", numJugadores=" + jugadores.size() + ", jugadores=" + jugadores + '}';
    }
}
