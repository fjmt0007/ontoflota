/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.util;


import es.uja.ssmmaa.ontologia.juegoTablero.Organizador;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author pedroj
 */
public class RegistroOrganizador {
    private final Organizador organizador;
    private final List<String> juegosOrganizados;

    public RegistroOrganizador(Organizador organizador) {
        this.organizador = organizador;
        this.juegosOrganizados = new LinkedList();
    }

    public Organizador getOrganizador() {
        return organizador;
    }

    public List<String> getJuegosOrganizados() {
        return juegosOrganizados;
    }
    
    public void addJuego(String idJuego) {
        juegosOrganizados.add(idJuego);
    }

    @Override
    public String toString() {
        return "RegistroOrganizador{" + "organizador=" + organizador + 
                ", juegosOrganizados=" + juegosOrganizados + '}';
    }
}
