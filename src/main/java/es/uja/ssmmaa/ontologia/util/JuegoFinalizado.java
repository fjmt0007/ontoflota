/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.util;


import static es.uja.ssmmaa.ontologia.Constantes.DIA;
import static es.uja.ssmmaa.ontologia.Constantes.DIVISOR;
import static es.uja.ssmmaa.ontologia.Constantes.IGUAL;
import static es.uja.ssmmaa.ontologia.Constantes.MAYOR;
import static es.uja.ssmmaa.ontologia.Constantes.MENOR;
import static es.uja.ssmmaa.ontologia.Constantes.NUM_JUEGO;
import es.uja.ssmmaa.ontologia.Vocabulario.Modo;
import es.uja.ssmmaa.ontologia.juegoTablero.ClasificacionJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.IncidenciaJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.Organizador;
import jade.util.leap.Serializable;
import java.util.Objects;

/**
 *
 * @author pedroj
 */
public class JuegoFinalizado implements Serializable, Comparable<JuegoFinalizado> {
    private final Juego juego;
    private final Organizador organizador;
    private final Modo modoJuego;
    private IncidenciaJuego incidencia;
    private ClasificacionJuego clasificacion;
    
    // Constantes
    private static final long serialVersionUID = 1L;

    public JuegoFinalizado(Juego juego, Organizador organizador, Modo modoJuego) {
        this.juego = juego;
        this.organizador = organizador;
        this.modoJuego = modoJuego;
        this.clasificacion = null;
        this.incidencia = null;
    }

    public String getIdJuego() {
        return juego.getIdJuego();
    }
    
    public Juego getJuego() {
        return juego;
    }
    
    public Organizador getOrganizador() {
        return organizador;
    }

    public Modo getModoJuego() {
        return modoJuego;
    }

    public ClasificacionJuego getClasificacion() {
        return clasificacion;
    }

    public IncidenciaJuego getIncidencia() {
        return incidencia;
    }
    
    public int getDia() {
        String[] elementos = this.getIdJuego().split(DIVISOR);
        return Integer.parseInt(elementos[DIA]);
    }
    
    public int getNumJuego() {
        String[] elementos = this.getIdJuego().split(DIVISOR);
        return Integer.parseInt(elementos[NUM_JUEGO]);
    }
    
    public void setIncidencia(IncidenciaJuego incidencia) {
        this.incidencia = incidencia;
    }

    public void setClasificacion(ClasificacionJuego clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    public boolean hayClasificacion() {
        return clasificacion != null;
    }
    
    public boolean hayIncidencia() {
        return incidencia != null;
    }
    
    public boolean juegoFinalizado() {
        return (clasificacion != null) || (incidencia != null);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JuegoFinalizado other = (JuegoFinalizado) obj;
        if (!Objects.equals(this.getIdJuego(), other.getIdJuego())) {
            return false;
        }
        return true;
        
    }

    @Override
    public int compareTo(JuegoFinalizado other) {
        int resultado;
        
        String[] elmThis = this.getIdJuego().split(DIVISOR);
        int diaThis = Integer.parseInt(elmThis[DIA]);
        int numJuegoThis = Integer.parseInt(elmThis[NUM_JUEGO]);
        
        String[] elmOther = other.getIdJuego().split(DIVISOR);
        int diaOther = Integer.parseInt(elmOther[DIA]);
        int numJuegoOther = Integer.parseInt(elmOther[NUM_JUEGO]);
        
        if ( diaThis < diaOther ) {
            resultado = MAYOR; 
        } else if ( diaThis > diaOther ) {
            resultado = MENOR;
        } else {
            if ( numJuegoThis < numJuegoOther ) {
                resultado = MAYOR;
            } else if ( numJuegoThis > numJuegoOther ) {
                resultado = MENOR;
            } else {
                resultado = IGUAL;
            }
        }
        
        return resultado;
    }

    @Override
    public String toString() {
        return "JuegoFinalizado{" + "juego=" + juego + "\norganizador=" + organizador + 
               "\nmodoJuego=" + modoJuego + "}\n";
    }
}
