/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.agentes;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import es.uja.ssmmaa.ontologia.Constantes;
import static es.uja.ssmmaa.ontologia.Constantes.TIME_OUT;
import es.uja.ssmmaa.ontologia.Constantes.TipoCasilla;
import static es.uja.ssmmaa.ontologia.Constantes.TipoCasilla.TORPEDO_BARCO;
import static es.uja.ssmmaa.ontologia.Constantes.aleatorio;
import static es.uja.ssmmaa.ontologia.Constantes.getNumBarcos;
import es.uja.ssmmaa.ontologia.Vocabulario;
import static es.uja.ssmmaa.ontologia.Vocabulario.JUEGOS;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoJuego.HUNDIR_LA_FLOTA;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.JUGADOR;
import es.uja.ssmmaa.ontologia.gui.AJugadorJFrame;
import es.uja.ssmmaa.ontologia.gui.ATableroJFrame;
import es.uja.ssmmaa.ontologia.hundirlaflota.BarcosColocados;
import es.uja.ssmmaa.ontologia.hundirlaflota.HundirLaFlota;
import es.uja.ssmmaa.ontologia.hundirlaflota.PartidaHundirLaFlota;
import es.uja.ssmmaa.ontologia.hundirlaflota.PedirBarcos;
import es.uja.ssmmaa.ontologia.hundirlaflota.Torpedo;
import es.uja.ssmmaa.ontologia.juegoTablero.EstadoPartida;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Movimiento;
import es.uja.ssmmaa.ontologia.juegoTablero.MovimientoEntregado;
import es.uja.ssmmaa.ontologia.juegoTablero.Partida;
import es.uja.ssmmaa.ontologia.juegoTablero.PedirMovimiento;
import es.uja.ssmmaa.ontologia.juegoTablero.Posicion;
import es.uja.ssmmaa.ontologia.juegoTablero.ProponerJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Tablero;
import es.uja.ssmmaa.ontologia.tareas.TareaPedirBarcosParticipante;
import es.uja.ssmmaa.ontologia.tareas.TareaPedirMovimiento;
import es.uja.ssmmaa.ontologia.tareas.TareaPedirMovimientoParticipante;
import es.uja.ssmmaa.ontologia.tareas.TareaPrepararMovimiento;
import es.uja.ssmmaa.ontologia.tareas.TareaProponerJuego;
import es.uja.ssmmaa.ontologia.tareas.TareaProponerJuegoParticipante;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.ACCEPT_PROPOSAL;
import static jade.lang.acl.ACLMessage.CFP;
import static jade.lang.acl.ACLMessage.PROPOSE;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Marin
 */
public class AgenteJugador extends Agent {

    private ContentManager manager;

    private final Codec codec = new SLCodec();

    private Ontology ontology;

    //GUI
    Map<String, ATableroJFrame> ifaces;

    //Variables del agente
    Map<String, PartidaHundirLaFlota> partidas;
    Map<String, ArrayList<Posicion>> historialMovimientos;
    Map<String, Juego> juegos;

    @Override
    protected void setup() {
        init();
        //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(JUGADOR.name());
        sd.setName(HUNDIR_LA_FLOTA.name());
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        //Registro de la Ontología
        try {
            ontology = Vocabulario.getOntology(HUNDIR_LA_FLOTA);
            manager = (ContentManager) getContentManager();
            manager.registerLanguage(codec);
            manager.registerOntology(ontology);
        } catch (BeanOntologyException ex) {
            //consola.addMensaje("Error al registrar la ontología \n" + ex);
            this.doDelete();
        }

        //Registro de las tareas
        MessageTemplate plantillaPropose;
        plantillaPropose = MessageTemplate.and(
                MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()),
                                MessageTemplate.MatchSender(this.getAMS()))),
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE));
        addBehaviour(new TareaProponerJuegoParticipante(this, plantillaPropose, manager));

        MessageTemplate plantillaCFP;
        plantillaCFP = MessageTemplate.and(
                MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()),
                                MessageTemplate.MatchSender(this.getAMS()))),
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET));
        addBehaviour(new TareaPedirMovimientoParticipante(this, plantillaCFP, manager));

        MessageTemplate template = MessageTemplate.and(
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
                MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

        MessageTemplate plantillaRequest;
        plantillaRequest = MessageTemplate.and(
                MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()),
                                MessageTemplate.MatchSender(this.getAMS()))),
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST));
        addBehaviour(new TareaPedirBarcosParticipante(this, template));

        System.out.println("Se inicia la ejecución del agente: " + this.getName());

    }

    private void init() {
        partidas = new HashMap<>();
        ifaces = new HashMap<>();
        juegos = new HashMap<>();
        historialMovimientos = new HashMap<>();
    }

    public void addPartida(Juego juego) {
        juegos.put(juego.getIdJuego(), juego);
        //partidas.put(phf.getIdPertida(), phf);
        //ifaces.put(phf.getIdPertida(), new AJugadorJFrame(this, phf));

    }

    public ACLMessage mover(Partida partida, Jugador jugador) throws Codec.CodecException, OntologyException, InterruptedException {
        Torpedo torpedo = new Torpedo(jugador, Constantes.TipoCasilla.TORPEDO);
        int num = Vocabulario.TipoJuego.HUNDIR_LA_FLOTA.getDimension();

        //Posicion posicion = new Posicion(aleatorio.nextInt(num), aleatorio.nextInt(num));
        //Obtenemos la posicion a enviar del registro de posiciones
        ArrayList<Posicion> historial = historialMovimientos.get(partida.getIdPartida());
        Posicion posicion = historial.get(historial.size() - 1);

        Movimiento mov = new Movimiento(torpedo, posicion);
        MovimientoEntregado movE = new MovimientoEntregado(partida, mov);

        //Mensaje a enviar
        ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
        msg.setSender(getAID());
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology.getName());
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));

        manager.fillContent(msg, movE);
        System.out.println(getLocalName() + ": Envio movimiento:" + mov);
        return msg;

    }

    public ACLMessage estadoPartida(Partida partida, ACLMessage recibido) throws Codec.CodecException, OntologyException {
        //Contenido del mensaje
        EstadoPartida seguirJugando = new EstadoPartida(partida, Vocabulario.Estado.SEGUIR_JUGANDO);
        EstadoPartida ganador = new EstadoPartida(partida, Vocabulario.Estado.GANADOR);

        //Mensaje a enviar
        ACLMessage msg = recibido.createReply();

        if (recibido.getPerformative() == CFP) {
            msg.setPerformative(PROPOSE);
            manager.fillContent(msg, seguirJugando);

        } else if (recibido.getPerformative() == ACCEPT_PROPOSAL) {
            if (ganador(partida.getIdPartida())) {
                manager.fillContent(msg, ganador);
            } else {
                manager.fillContent(msg, seguirJugando);
            }
            msg.setPerformative(ACLMessage.INFORM);
        }
        return msg;
    }

    public void aplicarMovimiento(MovimientoEntregado me) {
        String idPartida = me.getPartida().getIdPartida();
        Torpedo torpedo = (Torpedo) me.getMovimiento().getFicha();

        //Estructura
        PartidaHundirLaFlota phf = partidas.get(idPartida);
        phf.aplicarTorpedo(me.getMovimiento(), torpedo.getTipoCasilla());

        //Preparamos el siguiente movimiento una vez recibido el anterior
        if (torpedo.getLanzador().getAgenteJugador().compareTo(getAID()) == 0) { //El movimiento recibido ha sido lanzado por nosotros
            addBehaviour(new TareaPrepararMovimiento(this, phf.getTableroJ2(), idPartida));
        }

        //Interfaz
        ATableroJFrame ifaz = ifaces.get(idPartida);
        ifaz.torpedoLanzado(torpedo.getLanzador(), me.getMovimiento().getPosicion(), torpedo.getTipoCasilla());

//        System.out.println(getLocalName());
//        phf.printT2();
    }

    public BarcosColocados inicializarPartida(PedirBarcos pedirBarcos) {
        String idJuego = pedirBarcos.getPartida().getJuego().getIdJuego();
        if (juegos.containsKey(idJuego)) {
            PartidaHundirLaFlota phf = new PartidaHundirLaFlota(pedirBarcos.getJugador(), pedirBarcos.getRival(), pedirBarcos.getPartida(), false);
            partidas.put(pedirBarcos.getIdPartida(), phf);
            BarcosColocados bc = phf.inicializarTablero(pedirBarcos);
            ifaces.put(pedirBarcos.getIdPartida(), new ATableroJFrame(this, phf));

            //Al iniciar el juego preparamos nuestro primer movimiento
            addBehaviour(new TareaPrepararMovimiento(this, phf.getTableroJ2(), phf.getIdPartida()));

            return bc;
        } else {
            return null;
        }

    }

    public void addMovimiento(String idPartida, Posicion p) {
        if (!historialMovimientos.containsKey(idPartida)) {
            historialMovimientos.put(idPartida, new ArrayList<>());
        }
        historialMovimientos.get(idPartida).add(p);
    }

    private boolean ganador(String idPartida) {
        PartidaHundirLaFlota phf = partidas.get(idPartida);
        TipoCasilla[][] tablero = phf.getTableroJ2();
        int contBarcos = 0;
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                if (tablero[i][j] == TORPEDO_BARCO) {
                    contBarcos++;
                }
            }
        }

        return contBarcos == getNumBarcos();
    }

}
