/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.agentes;


import static es.uja.ssmmaa.ontologia.Constantes.ARCHIVO;
import static es.uja.ssmmaa.ontologia.Constantes.DIRECTORIO;
import static es.uja.ssmmaa.ontologia.Constantes.DIVISOR;
import static es.uja.ssmmaa.ontologia.Constantes.FINALIZACION;
import static es.uja.ssmmaa.ontologia.Constantes.MAX_JUGADORES;
import static es.uja.ssmmaa.ontologia.Constantes.MIN_JUGADORES;
import static es.uja.ssmmaa.ontologia.Constantes.PREFIJO_ID;
import static es.uja.ssmmaa.ontologia.Constantes.PRIMERO;
import static es.uja.ssmmaa.ontologia.Constantes.TIME_OUT;
import es.uja.ssmmaa.ontologia.Vocabulario;
import static es.uja.ssmmaa.ontologia.Vocabulario.JUEGOS;
import es.uja.ssmmaa.ontologia.Vocabulario.Modo;
import static es.uja.ssmmaa.ontologia.Vocabulario.TIPOS_SERVICIO;
import es.uja.ssmmaa.ontologia.Vocabulario.TipoJuego;
import es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.JUGADOR;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.ORGANIZADOR;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.TABLERO;
import es.uja.ssmmaa.ontologia.gui.ClasificacionJuegosJFrame;
import es.uja.ssmmaa.ontologia.gui.ConsolaJFrame;
import es.uja.ssmmaa.ontologia.gui.MonitorJFrame;
import es.uja.ssmmaa.ontologia.gui.RegistroAgentesJFrame;
import es.uja.ssmmaa.ontologia.hundirlaflota.HundirLaFlota;
import es.uja.ssmmaa.ontologia.juegoTablero.ClasificacionJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.CompletarJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.IncidenciaJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.InfoJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.InformarResultado;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Monitor;
import es.uja.ssmmaa.ontologia.juegoTablero.Organizador;
import es.uja.ssmmaa.ontologia.juegoTablero.ProponerJuego;
import es.uja.ssmmaa.ontologia.tareas.SubscripcionDF;
import es.uja.ssmmaa.ontologia.tareas.TareaInformarResultado;
import es.uja.ssmmaa.ontologia.tareas.TareaProponerJuego;
import es.uja.ssmmaa.ontologia.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.ontologia.tareas.TareasJuegoSub;
import es.uja.ssmmaa.ontologia.util.JuegoFinalizado;
import es.uja.ssmmaa.ontologia.util.RegistroJuego;
import es.uja.ssmmaa.ontologia.util.RegistroOrganizador;
import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.MicroRuntime;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionInitiator;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Javier Marin
 */
public class AgenteMonitor extends Agent implements SubscripcionDF, TareasJuegoSub{

    private ContentManager[] manager;
    
    private final Codec codec = new SLCodec();
    
    private Ontology[] ontology;
    
    //GUI
    private ConsolaJFrame consola;
    private RegistroAgentesJFrame registro;
    private MonitorJFrame gui;
    private ClasificacionJuegosJFrame guiClasificacion;
    
    //Variables del agente
    private Map<String,Deque<AID>> agentesConocidos;
    private Map<String,RegistroJuego> juegosCompletos;
    private Map<String,RegistroJuego> juegosPendientes;
    private Map<String,JuegoFinalizado> resultados;
    private Map<String,RegistroOrganizador> registroOrganizadores;
    private Map<String,TareaInformarResultado> subActivas;
    private Set<Vocabulario.TipoJuego> conjuntoJuegos;
    private Monitor monitor;
    private String diaJuego;
    private int numJuego;

@Override
    protected void setup() {
        // Iniciacilación de las variables
        init();
        
        //Configuración del GUI
        consola = new ConsolaJFrame(this);
        registro = new RegistroAgentesJFrame(this);
        gui = new MonitorJFrame(this);
        guiClasificacion = new ClasificacionJuegosJFrame(this);
        
        
        try {
            //Se cargan los juegos finalizados anteriormente
            cargarDatos();
        } catch (Exception ex) {
            numJuego = 0;
            resultados = new HashMap();
            System.out.println("Resultados: " + resultados);
            consola.addMensaje("Inicializamos el registro de juegos finalizados\n" + ex 
                    + "\ndia " + diaJuego + " numJuego " + (numJuego+1) + "\n" + resultados);
        }
       
        //Registro del agente en las Páginas Amarrillas
       
        //Registro de la Ontología
        ontology = new Ontology[JUEGOS.length];
        manager = new ContentManager[JUEGOS.length];
        try {
            for (int i = 0; i < JUEGOS.length; i++) {
                ontology[i] = Vocabulario.getOntology(JUEGOS[i]);
                manager[i] = (ContentManager) getContentManager();
                manager[i].registerLanguage(codec);
                manager[i].registerOntology(ontology[i]);
            }
        } catch (BeanOntologyException ex) {
            consola.addMensaje("Error al registrar la ontología \n" + ex);
            this.doDelete();
        }
        System.out.println("Se inicia la ejecución del agente: " + this.getName());
       
        //Añadir las tareas principales
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription templateSd = new ServiceDescription();
        templateSd.setType(JUGADOR.name());
        template.addServices(templateSd);
        addBehaviour(new TareaSubscripcionDF(this,template));
        templateSd.setType(TABLERO.name());
        template.addServices(templateSd);
        addBehaviour(new TareaSubscripcionDF(this,template));
        templateSd.setType(ORGANIZADOR.name());
        template.addServices(templateSd);
        addBehaviour(new TareaSubscripcionDF(this,template));
        
        
    }
    
    @Override
    protected void takeDown() {
        //Eliminar registro del agente en las Páginas Amarillas
       
        //Eliminamos las suscripciones activas del agente monitor con los
        //organizadores
        for( RegistroOrganizador organizador : registroOrganizadores.values() )
            cancelarSub(organizador.getOrganizador().getAgenteOrganizador());
        
        try {
            //Almacenamos los resultados
            guardarDatos();
        } catch (IOException ex) {
            consola.addMensaje("ERROR AL ALMACENAR LOS DATOS DE PARTIDAS");
        } 
        
        try {
            // Esperamos un tiempo antes de finalizar
            consola.addMensaje("FINALIZA LA EJECUCIÓN");
            TimeUnit.SECONDS.sleep(FINALIZACION);
        } catch (InterruptedException ex) {
            consola.addMensaje("INTERRUPCIÓN EN LA FINALIZACIÓN\n" + ex);
        }
        
        //Liberación de recursos, incluido el GUI
        consola.dispose();
        registro.dispose();
        gui.dispose();
        guiClasificacion.dispose();
        
        //Despedida
        System.out.println("Finaliza la ejecución del agente: " + this.getName());
        MicroRuntime.stopJADE();
    }
    
    //Métodos de trabajo del agente
    private void init() {
        monitor = new Monitor(this.getLocalName(),this.getAID());
        agentesConocidos = new HashMap();
        for(TipoServicio servicio : TIPOS_SERVICIO)
            for(TipoJuego juego : JUEGOS)
                agentesConocidos.put(servicio.name()+juego.name(), new LinkedList());
       
        conjuntoJuegos = new HashSet();
        juegosCompletos = new HashMap();
        juegosPendientes = new HashMap();
        registroOrganizadores = new HashMap();
        subActivas = new HashMap();
    }
    
    private void cargarDatos() throws Exception {
        // Carga de los juegos almacenados
        SimpleDateFormat sdf = new SimpleDateFormat(PREFIJO_ID);
        diaJuego = sdf.format(new Date()).toString();
        int dia = Integer.parseInt(diaJuego);
        File directorio = new File(DIRECTORIO);
        File archivo = new File(DIRECTORIO+ARCHIVO);
        
        
        if ( archivo.exists() ) {
            FileInputStream fis = new FileInputStream(archivo);
            BufferedInputStream bis = new BufferedInputStream(fis);
            ObjectInputStream ois = new ObjectInputStream(bis);
            resultados = (Map<String,JuegoFinalizado>) ois.readObject();
            ois.close();
            
            if ( resultados != null ) {
                ArrayList<JuegoFinalizado> listaJuegos = new ArrayList(resultados.values());
                Collections.sort(listaJuegos);
                JuegoFinalizado primero = listaJuegos.get(PRIMERO);
                if ( dia == primero.getDia() ) {
                    numJuego = primero.getNumJuego();
                } else {
                    numJuego = 0;
                }
                
                // Presentamos los resultados
                for(JuegoFinalizado resultado : listaJuegos)
                    guiClasificacion.addJuego(resultado);
                
                consola.addMensaje("CARGA DE LOS RESULTADOS GUARDADOS" +
                        "\nEl últmo día almacenado " + primero.getDia() + "\nel día de hoy: " + 
                        dia + "\nel siguiente juego para el día es: " + (numJuego+1) +
                        "\ndivision " + Arrays.toString(primero.getIdJuego().split(DIVISOR)));
            } else {
                throw new NullPointerException("No hay resultados almacenados");
            }
        } else {
            directorio.mkdir();
            throw new FileNotFoundException("No hay fichero de datos");
        }
    }
    
    private void guardarDatos() throws FileNotFoundException, IOException {
        File archivo = new File(DIRECTORIO+ARCHIVO);
        
        FileOutputStream fos = new FileOutputStream(archivo);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        
        oos.writeObject(resultados);
        oos.close();
        consola.addMensaje("Se ha guardado la información de los juegos finalizados");
    }
    
    @Override
    public void addAgent(AID agente, Vocabulario.TipoJuego juego, TipoServicio tipoServicio) {
        //TipoServicio servicio = buscarServicio(tipoServicio);
        
        Deque<AID> agentesJuego = agentesConocidos.get(tipoServicio.name()+juego.name());
        agentesJuego.add(agente);
       
        registro.addAgentes(juego, tipoServicio, agentesJuego);
        
        switch ( tipoServicio ) {
                case JUGADOR:
                    // Comprobamos is hay agentes jugadores para el juego
                    conjuntoJuegos.add(juego);
                    gui.proponerJuego(juego);
                    break;
                case ORGANIZADOR:
                    // Añadimos el agente registroOrganizador a la interface 
                    gui.addOrganizador(agente, juego);
                    break;
            }
    }
    
    @Override
    public boolean removeAgent(AID agente, Vocabulario.TipoJuego juego, TipoServicio tipoServicio) {
        boolean resultado = false;
        //TipoServicio servicio = buscarServicio(tipoServicio);
        
        Deque<AID> agentesJuego = agentesConocidos.get(tipoServicio.name()+juego.name());
        resultado = agentesJuego.remove(agente);
        
        if( resultado ) {
            registro.addAgentes(juego, tipoServicio, agentesJuego);
            
            switch ( tipoServicio ) {
                case JUGADOR:
                    // Comprobamos is hay agentes jugadores para el juego
                    conjuntoJuegos.remove(juego);
                    ajustarJuegos(agente);
                    gui.proponerJuego(juego);
                    break;
                case ORGANIZADOR:
                    // Eliminamos el agente registroOrganizador de la interface 
                    gui.removeOrganizador(agente, juego);
                    cancelarSub(agente);
                    break;
            }
        }
        
        return resultado;
    }   
    
    private void ajustarJuegos(AID agente) {
        // Comprobamos primero los juegos completos
        for( RegistroJuego registro : juegosCompletos.values() ) {
            Jugador jugador = registro.getJugador(agente);
            if( registro.removeJugador(jugador) )
                consola.addMensaje("Eliminado el jugador " + jugador +
                                   "\nREGISTRO JUEGO\n " + registro);
            
            // Comprobamos si el juego tiene suficientes jugadores
            this.setRegistroJuego(registro.getJuego().getIdJuego(), registro);
        }
        
        // Comprobamos ahora los juegos incompletos
        for( RegistroJuego registro : juegosPendientes.values() ) {
            Jugador jugador = registro.getJugador(agente);
            if( registro.removeJugador(jugador) )
                consola.addMensaje("Eliminado el jugador " + jugador +
                                   "\nREGISTRO JUEGO\n " + registro);
        }
    }
    
    
    /**
     * Se cancela la suscripción para informar del resultado de un juego
     * con el agente organizador
     * @param agente 
     */
    private void cancelarSub(AID agente) {
        TareaInformarResultado sub = subActivas.remove(agente.getLocalName());
        if( sub != null ) {
            sub.cancel(agente, true);
            consola.addMensaje("SUSBCRIPCIÓN CANDELADA PARA\n" + agente);
        }
    }
    
    public boolean hayJugadores(TipoJuego juego, Modo modo) {
        int minimo = MIN_JUGADORES[modo.ordinal()];
        return agentesConocidos.get(JUGADOR.name()+juego.name()).size() >= minimo;
    }
    
    
    public void proponerJuego(TipoJuego tipoJuego, Modo modoJuego) {
        RegistroJuego registroJuego;
        ProponerJuego proponerJuego;
        Juego juego;
        InfoJuego infoJuego;
        numJuego++;
        
        // Contenido del mensaje representado en la ontología
        String idJuego = tipoJuego.name() + "-" + diaJuego + "-"+ numJuego;
        juego = new Juego(idJuego, tipoJuego);
        infoJuego = getInfoJuego(tipoJuego);
        proponerJuego = new ProponerJuego(juego,modoJuego,infoJuego);
        registroJuego = new RegistroJuego(juego,modoJuego);
                
        // Creamos el mensaje a enviar
        ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
        msg.setSender(getAID());
        int numJugadores = addAgentesJugador(msg, registroJuego);
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology[tipoJuego.ordinal()].getName());
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        
        Action ac = new Action(this.getAID(), proponerJuego);
        
        try {
            // Completamos en contenido del mensajes
            manager[tipoJuego.ordinal()].fillContent(msg, ac);
        } catch (Codec.CodecException | OntologyException ex) {
            consola.addMensaje("Error en la construcción del mensaje en Proponer Juego \n" + ex);
        }
        
        // Creamos la estructura para llevar el registro de los jugadores
        juegosCompletos.put(idJuego, registroJuego);
        consola.addMensaje("MENSAJE ENVIADO\n" + msg);
        consola.addMensaje("REGISTRO JUEGO\n" + registroJuego + 
                           "\nPeticiones solicitadas :" + numJugadores);
        
        // Se añade la tareaSub para completar el regisro de jugadores para el juego
        this.addBehaviour(new TareaProponerJuego(this,msg,juego,JUGADOR));
    }
    
    public void completarJuego(String idJuego, TipoJuego tipoJuego) {
        RegistroJuego registroJuego;
        ProponerJuego proponerJuego;
        Juego juego;
        Modo modoJuego;
        InfoJuego infoJuego;
        
        // Contenido del mensaje representado por la ontología
        registroJuego = juegosPendientes.get(idJuego);
        juego = registroJuego.getJuego();
        modoJuego = registroJuego.getModoJuego();
        infoJuego = getInfoJuego(tipoJuego);
        proponerJuego = new ProponerJuego(juego,modoJuego,infoJuego);
        
        // Creamos el mensaje a enviar
        ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
        msg.setSender(getAID());
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology[tipoJuego.ordinal()].getName());
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        int numJugadores = addAgentesJugador(msg, registroJuego);
        
        if( numJugadores != 0 ) {
            Action ac = new Action(this.getAID(), proponerJuego);
        
            try {
                // Completamos en contenido del mensajes
                manager[tipoJuego.ordinal()].fillContent(msg, ac);
            } catch (Codec.CodecException | OntologyException ex) {
                consola.addMensaje("Error en la construcción del mensaje en Proponer Juego \n" + ex);
            }
        
            // Creamos la estructura para llevar el registro de los jugadores
            juegosCompletos.put(idJuego, registroJuego);
            consola.addMensaje("MENSAJE ENVIADO\n" + msg);
            consola.addMensaje("REGISTRO JUEGO\n" + registroJuego + 
                           "\nPeticiones solicitadas :" + numJugadores);
        
            // Se añade la tareaSub para completar el regisro de jugadores para el juego
            this.addBehaviour(new TareaProponerJuego(this,msg,juego,JUGADOR));
        } else {
            gui.addJuegoIncompleto(idJuego, tipoJuego, modoJuego);
            consola.addMensaje("NO HAY MÁS JUGADORS DISPONIBLES POR AHORA");
        }
    }
    
    public void finalizarJuego(String idJuego, AID agenteOrganizador) {
        RegistroJuego registroJuego;
        CompletarJuego completarJuego;
        Juego juego;
        Modo modoJuego;
        InfoJuego infoJuego;
        jade.util.leap.List listaJugadores;
        
        // Creamos el contenido del mensaje representado por la ontología
        registroJuego = juegosCompletos.get(idJuego);
        juego = registroJuego.getJuego();
        modoJuego = registroJuego.getModoJuego();
        infoJuego = getInfoJuego(juego.getTipoJuego());
        listaJugadores = new jade.util.leap.ArrayList((ArrayList) registroJuego.getJugadores());
        completarJuego = new CompletarJuego(juego,modoJuego,infoJuego,listaJugadores);
        
        // Creamos el mensaje
        ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
        msg.setSender(getAID());
        msg.addReceiver(agenteOrganizador);
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology[juego.getTipoJuego().ordinal()].getName());
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        
        Action ac = new Action(this.getAID(), completarJuego);
        try {
            // Completamos en contenido del mensajes
            manager[juego.getTipoJuego().ordinal()].fillContent(msg, ac);
        } catch (Codec.CodecException | OntologyException ex) {
            consola.addMensaje("Error en la construcción del mensaje en Proponer Juego \n" + ex);
        }
        
        consola.addMensaje("COMPLETAR JUEGO\n" + msg);
        consola.addMensaje("ID Juego: " + idJuego + "\nOrganizador: " + agenteOrganizador);
        
        // Se añade la tareaSub para completar el regisro de jugadores para el juego
        this.addBehaviour(new TareaProponerJuego(this,msg,juego,ORGANIZADOR));
    }
    
    private void informarResultado(AID agenteOrganizador, TipoJuego tipoJuego) {
        String nameAgente = agenteOrganizador.getLocalName();
        
        if( !subActivas.containsKey(nameAgente) ) {
            InformarResultado resultado = new InformarResultado(monitor);
            
            //Creamos el mensaje para lanzar el protocolo Subscribe
            ACLMessage msg = new ACLMessage(ACLMessage.SUBSCRIBE);
            msg.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
            msg.setSender(this.getAID());
            msg.setLanguage(codec.getName());
            msg.setOntology(ontology[tipoJuego.ordinal()].getName());
            msg.addReceiver(agenteOrganizador);
            System.out.println("enviando peticion de suscripcion a " + agenteOrganizador);
            msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        
            Action ac = new Action(this.getAID(), resultado);
        
            try {
                manager[tipoJuego.ordinal()].fillContent(msg, ac);
            } catch (Codec.CodecException | OntologyException ex) {
                consola.addMensaje("Error en la construcción del mensaje en Informar Resultado \n" + ex);
            }
        
            addBehaviour(new TareaInformarResultado(this,msg));
        } else 
            consola.addMensaje("Ya hay una suscripción activa con " + nameAgente);
    }
    
    private InfoJuego getInfoJuego(TipoJuego tipoJuego) {
        InfoJuego infoJuego = null;
        
        switch (tipoJuego) { // Condiciones estandar para el juego
            
            case HUNDIR_LA_FLOTA:
                infoJuego=new HundirLaFlota();
            /*
            case TRES_EN_RAYA:
                infoJuego = new TresEnRaya();
                break;
            case QUORIDOR:
                infoJuego = new Quoridor();
                break;
            case QUATRO:
                infoJuego = new Quatro();
                break;
            */
        }
        
        return infoJuego;
    }
    
    
    private int addAgentesJugador(ACLMessage msg, RegistroJuego registro) {
        TipoJuego juego = registro.getJuego().getTipoJuego();
        Modo modo = registro.getModoJuego();
        int i = registro.numJugadores();
        Deque<AID> listaJugadores = agentesConocidos.get(JUGADOR.name()+juego.name());
        int maxJugadores = listaJugadores.size();
        
        while( (i < MAX_JUGADORES[modo.ordinal()]) && (i < maxJugadores) ) {
            AID agenteJugador = listaJugadores.removeFirst();
            
            // Comprobamos que no se encuentan ya en el registro del juego
            if( !registro.juadorRegistrado(agenteJugador) )
                msg.addReceiver(agenteJugador);
            
            i++;
            
            // Lo colocamos al final de la lista
            listaJugadores.addLast(agenteJugador);
        }
        
        return i - registro.numJugadores();
    }
    
    @Override
    public ContentManager getManager(TipoJuego tipoJuego) {
        return manager[tipoJuego.ordinal()];
    }

    @Override
    public Ontology getOntology(TipoJuego tipoJuego) {
        return ontology[tipoJuego.ordinal()];
    }
    
    @Override
    public ContentManager getManager(String nameOntology) {
        for(int i = 0; i < ontology.length; i++)
            if(ontology[i].getName().equals(nameOntology))
                return manager[i];
        
        return null;
    }
    
    @Override
    public Ontology getOntology(String nameOntology) {
        for(Ontology onto : ontology)
            if(onto.getName().equals(nameOntology))
                return onto;
        
        return null;
    }

    @Override
    public void addMsgConsola(String msg) {
        consola.addMensaje(msg);
    }
    
    @Override
    public void setRegistroJuego(String idJuego, RegistroJuego registro) {
        Modo modo = registro.getModoJuego();
        TipoJuego tipoJuego = registro.getJuego().getTipoJuego();
        
        if( registro.registroCompleto() ) {
            juegosCompletos.put(idJuego, registro);
            gui.addJuegoDisponible(idJuego, tipoJuego, modo);
            juegosPendientes.remove(idJuego);
        } else {
            juegosCompletos.remove(idJuego);
            gui.removeJuegoDisponible(idJuego, tipoJuego, modo);
            juegosPendientes.put(idJuego,registro);
            gui.addJuegoIncompleto(idJuego, tipoJuego, modo);
        }
    }
    
    @Override
    public RegistroJuego getRegistroJuego(String idJuego) {
        return juegosCompletos.get(idJuego);
    }
    
    @Override
    public void addJuego(Juego juego, Organizador organizador) {
        String name = organizador.getNombre();
        RegistroOrganizador registroOrganizador = registroOrganizadores.get(name);
        RegistroJuego registroJuego = juegosCompletos.get(juego.getIdJuego());
        
        if( registroOrganizador == null ) {
            registroOrganizador = new RegistroOrganizador(organizador);
            registroOrganizadores.put(name, registroOrganizador);
        }
        
        registroOrganizador.addJuego(juego.getIdJuego());
        this.informarResultado(organizador.getAgenteOrganizador(), juego.getTipoJuego());
        gui.removeJuegoDisponible(juego.getIdJuego(), juego.getTipoJuego(), registroJuego.getModoJuego());
        consola.addMensaje("El " + organizador + 
                           "\nAcepta la organizador del juego " + juego.getIdJuego() +
                           "\nLista total " + registroOrganizador.getJuegosOrganizados());
        
        // A la espera para recibir el resultado del juego por el organizador
        // Se elimina de los juegos que están completos y a la espera de organizador
        Modo modoJuego = juegosCompletos.remove(juego.getIdJuego()).getModoJuego();
        JuegoFinalizado resultado = new JuegoFinalizado(juego, organizador, modoJuego);
        resultados.put(juego.getIdJuego(), resultado);
        guiClasificacion.addJuego(resultado);
    }
    
    @Override
    public void addSubcription(String nameAgente, SubscriptionInitiator sub) {
        // Añadimos la tareaSub de suscripción
        subActivas.put(nameAgente, (TareaInformarResultado) sub);
    }

    @Override
    public void setResultado(AID agenteOrganizador, ContentElement resultado) {
        JuegoFinalizado finJuego;
        
        if( resultado instanceof ClasificacionJuego ) {
            ClasificacionJuego clasificacion = (ClasificacionJuego) resultado;
            finJuego = resultados.get(clasificacion.getJuego().getIdJuego());
            finJuego.setClasificacion(clasificacion);
            guiClasificacion.addClasificacion(clasificacion);
            consola.addMensaje("Añadida clasificación: " + clasificacion.getJuego());
        } else if ( resultado instanceof IncidenciaJuego ) {
            IncidenciaJuego incidencia = (IncidenciaJuego) resultado;
            finJuego = resultados.get(incidencia.getJuego().getIdJuego());
            finJuego.setIncidencia(incidencia);
            guiClasificacion.addIncidencia(incidencia);
            consola.addMensaje("Añadida incidencia: " + incidencia.getJuego());
        } else {
            consola.addMensaje("El organizador " + agenteOrganizador.getLocalName() +
                               "\nNo ha enviado correctamente el resultado del juego");
        }
    }
    
    
    
    
    
}
