/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.agentes;

import static es.uja.ssmmaa.ontologia.Constantes.MAX_JUGADORES;
import static es.uja.ssmmaa.ontologia.Constantes.TIME_OUT;
import es.uja.ssmmaa.ontologia.Vocabulario;
import static es.uja.ssmmaa.ontologia.Vocabulario.JUEGOS;
import es.uja.ssmmaa.ontologia.Vocabulario.Modo;
import static es.uja.ssmmaa.ontologia.Vocabulario.TIPOS_SERVICIO;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoJuego.HUNDIR_LA_FLOTA;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.JUGADOR;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.ORGANIZADOR;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.TABLERO;
import es.uja.ssmmaa.ontologia.gui.ConsolaJFrame;
import es.uja.ssmmaa.ontologia.hundirlaflota.HundirLaFlota;
import es.uja.ssmmaa.ontologia.juegoTablero.CompletarJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.CompletarPartida;
import es.uja.ssmmaa.ontologia.juegoTablero.InfoJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.Organizador;
import es.uja.ssmmaa.ontologia.juegoTablero.Partida;
import es.uja.ssmmaa.ontologia.juegoTablero.Tablero;
import es.uja.ssmmaa.ontologia.tareas.SubscripcionDF;
import es.uja.ssmmaa.ontologia.tareas.TareaInformarResultadoParticipante;
import es.uja.ssmmaa.ontologia.tareas.TareaProponerJuego;
import es.uja.ssmmaa.ontologia.tareas.TareaProponerJuegoParticipante;
import es.uja.ssmmaa.ontologia.tareas.TareaSubscripcionDF;
import es.uja.ssmmaa.ontologia.tareas.TareasJuego;
import es.uja.ssmmaa.ontologia.util.GestorSubscripciones;
import es.uja.ssmmaa.ontologia.util.RegistroJuego;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.PROPOSE;
import jade.lang.acl.MessageTemplate;
import jade.tools.gui.ACLAIDDialog;
import jade.util.leap.ArrayList;
import jade.util.leap.List;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Marin
 */
public class AgenteOrganizador extends Agent implements SubscripcionDF,TareasJuego {

    private ContentManager manager;

    private final Codec codec = new SLCodec();

    private Ontology ontology;

    //GUI
    ConsolaJFrame consola;
    //Variables del agente
    private GestorSubscripciones gestorSubs;
    private Map<String, RegistroJuego> juegosCompletos;
    private Map<String, RegistroJuego> juegosPendientes;
    private Map<String, Deque<AID>> tablerosConocidos;
    private int contadorPartidas = 0;

    @Override
    protected void setup() {
        //Variables del agente
        gestorSubs = new GestorSubscripciones();
        juegosCompletos = new HashMap<>();
        juegosPendientes = new HashMap<>();
        init();

        //Inicializacion gui
        consola = new ConsolaJFrame(this);

        //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(ORGANIZADOR.name());
        sd.setName(HUNDIR_LA_FLOTA.name());
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        //Registro de la Ontología
        try {
            ontology = Vocabulario.getOntology(HUNDIR_LA_FLOTA);
            manager = (ContentManager) getContentManager();
            manager.registerLanguage(codec);
            manager.registerOntology(ontology);
        } catch (BeanOntologyException ex) {
            //consola.addMensaje("Error al registrar la ontología \n" + ex);
            this.doDelete();
        }

        //Registro de las tareas
        //Tarea proponer juego participante
        MessageTemplate plantillaPropose;
        plantillaPropose = 
                MessageTemplate.and(
                MessageTemplate.and(
                MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()),
                                MessageTemplate.MatchSender(this.getAMS()))),
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE)),
                MessageTemplate.MatchPerformative(ACLMessage.PROPOSE));
        addBehaviour(new TareaProponerJuegoParticipante(this, plantillaPropose, manager));

        //Tarea informar resultado participante
        MessageTemplate plantillaSubscribe;
        plantillaSubscribe = 
                
                MessageTemplate.and(
                    MessageTemplate.not(
                        MessageTemplate.or(
                                MessageTemplate.MatchSender(this.getDefaultDF()),
                                MessageTemplate.MatchSender(this.getAMS())
                        )
                    ),
                    MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE));
                
        addBehaviour(new TareaInformarResultadoParticipante(this, plantillaSubscribe, gestorSubs, manager));

        //Suscripcion a paginas amarillas para encontrar agentes tablero
        //Añadir las tareas principales
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription templateSd = new ServiceDescription();
        templateSd.setType(TABLERO.name());
        template.addServices(templateSd);
        addBehaviour(new TareaSubscripcionDF(this, template));

        System.out.println("Se inicia la ejecución del agente: " + this.getName());

    }
    
    private void init(){
        tablerosConocidos = new HashMap();
            for(Vocabulario.TipoJuego juego : JUEGOS)
                tablerosConocidos.put(juego.name(), new LinkedList());
    }

    public void addJuego(Juego juego, CompletarJuego completarJuego) throws Codec.CodecException, OntologyException {
        Modo modo = completarJuego.getModo();
        RegistroJuego registroJuego = new RegistroJuego(juego, modo);
        juegosPendientes.put(juego.getIdJuego(), registroJuego);
        consola.addMensaje("Juego añadido " + juego);
        proponerTablero(completarJuego);

    }
    
    private String generarIdPartida(){
        contadorPartidas++;
        return this.getLocalName()+"-Partida-"+contadorPartidas;
        
    }

    public void proponerTablero(CompletarJuego cj) throws Codec.CodecException, OntologyException {
        //Contenido del mensaje
        Partida partida = new Partida(generarIdPartida(), cj.getJuego(), 1, 3);
        CompletarPartida cp = new CompletarPartida(partida, new HundirLaFlota(), cj.getListaJugadores());
        
        //Mensaje a enviar
        ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
        msg.setSender(getAID());
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology.getName());
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        AID aidTablero= this.tablerosConocidos.get(HUNDIR_LA_FLOTA.name()).getFirst();
        msg.addReceiver(aidTablero);
        
        //Codificamos el mensaje
        Action ac = new Action(this.getAID(), cp);
        manager.fillContent(msg, ac);
        this.addBehaviour(new TareaProponerJuego(this, msg, cj.getJuego(), TABLERO));
        
    }

    @Override
    public void addAgent(AID agente, Vocabulario.TipoJuego juego, Vocabulario.TipoServicio tipoServicio) {
        Deque<AID> agentesJuego = tablerosConocidos.get(juego.name());
        agentesJuego.add(agente);
    }

    @Override
    public boolean removeAgent(AID agente, Vocabulario.TipoJuego juego, Vocabulario.TipoServicio tipoServicio) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return true;
    }

    @Override
    public ContentManager getManager(Vocabulario.TipoJuego tipoJuego) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return manager;
    }

    @Override
    public ContentManager getManager(String nameOntology) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return manager;
    }

    @Override
    public Ontology getOntology(Vocabulario.TipoJuego tipoJuego) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return null;
    }

    @Override
    public Ontology getOntology(String nameOntology) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return null;
    }

    @Override
    public void addMsgConsola(String msg) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setRegistroJuego(String idJuego, RegistroJuego registro) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public RegistroJuego getRegistroJuego(String idJuego) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return null;
    }

    @Override
    public void addJuego(Juego juego, Organizador agenteOrganizador) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
