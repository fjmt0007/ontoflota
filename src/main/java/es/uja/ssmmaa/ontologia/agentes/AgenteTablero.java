/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.ontologia.agentes;

import es.uja.ssmmaa.ontologia.Constantes;
import static es.uja.ssmmaa.ontologia.Constantes.TIME_OUT;
import es.uja.ssmmaa.ontologia.Constantes.TipoCasilla;
import es.uja.ssmmaa.ontologia.Vocabulario;
import static es.uja.ssmmaa.ontologia.Vocabulario.JUEGOS;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoJuego.HUNDIR_LA_FLOTA;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.JUGADOR;
import static es.uja.ssmmaa.ontologia.Vocabulario.TipoServicio.TABLERO;
import es.uja.ssmmaa.ontologia.gui.ATableroJFrame;
import es.uja.ssmmaa.ontologia.hundirlaflota.BarcosColocados;
import es.uja.ssmmaa.ontologia.hundirlaflota.HundirLaFlota;
import es.uja.ssmmaa.ontologia.hundirlaflota.PartidaHundirLaFlota;
import es.uja.ssmmaa.ontologia.hundirlaflota.PedirBarcos;
import es.uja.ssmmaa.ontologia.hundirlaflota.Peticion;
import es.uja.ssmmaa.ontologia.hundirlaflota.Torpedo;
import es.uja.ssmmaa.ontologia.juegoTablero.CompletarJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.CompletarPartida;
import es.uja.ssmmaa.ontologia.juegoTablero.InfoJuego;
import es.uja.ssmmaa.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.ontologia.juegoTablero.Jugador;
import es.uja.ssmmaa.ontologia.juegoTablero.MovimientoEntregado;
import es.uja.ssmmaa.ontologia.juegoTablero.Organizador;
import es.uja.ssmmaa.ontologia.juegoTablero.Partida;
import es.uja.ssmmaa.ontologia.juegoTablero.PedirMovimiento;
import es.uja.ssmmaa.ontologia.juegoTablero.Tablero;
import es.uja.ssmmaa.ontologia.tareas.TareaGestionarPeticiones;
import es.uja.ssmmaa.ontologia.tareas.TareaPedirBarcos;
import es.uja.ssmmaa.ontologia.tareas.TareaPedirMovimiento;
import es.uja.ssmmaa.ontologia.tareas.TareaPintarTablero;
import es.uja.ssmmaa.ontologia.tareas.TareaProponerJuego;
import es.uja.ssmmaa.ontologia.tareas.TareaProponerJuegoParticipante;
import es.uja.ssmmaa.ontologia.tareas.TareasJuego;
import es.uja.ssmmaa.ontologia.tareas.TareasPintarTablero;
import es.uja.ssmmaa.ontologia.util.RegistroJuego;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.IOException;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Marin
 */
public class AgenteTablero extends Agent implements TareasJuego, TareasPintarTablero {

    private ContentManager manager;

    private final Codec codec = new SLCodec();

    private Ontology ontology;

    //GUI
    Map<String, ATableroJFrame> ifaces;
    //Variables del agente
    Map<String, PartidaHundirLaFlota> tableros;
    Map<String, Partida> partidas;
    Queue<MovimientoEntregado> movimientosCola;
    Queue<Peticion> peticionesCola;
    boolean ocupado;

    @Override
    protected void setup() {

        //Iniciando variables del agente
        tableros = new HashMap<>();
        partidas = new HashMap<>();
        movimientosCola = new LinkedList<>();
        peticionesCola = new LinkedList<>();
        ocupado = false;

        //Inicio gui
        ifaces = new HashMap<>();

        //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(TABLERO.name());
        sd.setName(HUNDIR_LA_FLOTA.name());
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        //Registro de la Ontología
        try {
            ontology = Vocabulario.getOntology(HUNDIR_LA_FLOTA);
            manager = (ContentManager) getContentManager();
            manager.registerLanguage(codec);
            manager.registerOntology(ontology);
        } catch (BeanOntologyException ex) {
            //consola.addMensaje("Error al registrar la ontología \n" + ex);
            this.doDelete();
        }

        //Registro de las tareas
        MessageTemplate plantilla;
        plantilla = MessageTemplate.and(
                MessageTemplate.not(
                        MessageTemplate.or(MessageTemplate.MatchSender(this.getDefaultDF()),
                                MessageTemplate.MatchSender(this.getAMS()))),
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE));
        addBehaviour(new TareaProponerJuegoParticipante(this, plantilla, manager));

        addBehaviour(new TareaPintarTablero(this));
        addBehaviour(new TareaGestionarPeticiones(this));

        System.out.println("Se inicia la ejecución del agente: " + this.getName());

    }

    public ContentManager getManager() {
        return manager;
    }

    public void addPartida(CompletarPartida completarPartida) throws Codec.CodecException, OntologyException {
        Jugador j1 = (Jugador) completarPartida.getListaJugadores().get(0);
        Jugador j2 = (Jugador) completarPartida.getListaJugadores().get(1);

        Partida partida = completarPartida.getPartida();
        HundirLaFlota hundirLaFlota = (HundirLaFlota) completarPartida.getInfoJuego();

        int anchura = hundirLaFlota.getColumnas();
        int altura = hundirLaFlota.getFilas();
        PartidaHundirLaFlota partidaHundirLaFlota = new PartidaHundirLaFlota(j1, j2, partida,true);
        System.out.println("Anadiendo partida: " + partida.getIdPartida());
        tableros.put(partida.getIdPartida(), partidaHundirLaFlota);
        partidas.put(partida.getIdPartida(), completarPartida.getPartida());
        //ifaces.put(partida.getIdPartida(), new ATableroJFrame(this, partidaHundirLaFlota));
        //peticionesCola.add(new Peticion(partida.getIdPartida(), j1.getAgenteJugador()));
        pedirColocarBarcos(partida.getIdPartida());
    }

    public void pedirColocarBarcos(String idPartida) throws Codec.CodecException, OntologyException {
        PartidaHundirLaFlota phf = tableros.get(idPartida);
        PedirBarcos pb1 = new PedirBarcos(phf.getPartida(), phf.getJ1(),phf.getJ2());

        Action accion1 = new Action(this.getAID(), pb1);

        ACLMessage msg1 = new ACLMessage(ACLMessage.REQUEST);
        msg1.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        msg1.setSender(getAID());
        msg1.setLanguage(codec.getName());
        msg1.setOntology(ontology.getName());
        msg1.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        msg1.addReceiver(phf.getJ1().getAgenteJugador());

        manager.fillContent(msg1, accion1);
        addBehaviour(new TareaPedirBarcos(this, msg1));

        //----------------------------------------------------
        PedirBarcos pb2 = new PedirBarcos(phf.getPartida(), phf.getJ2(),phf.getJ1());
        Action accion2 = new Action(this.getAID(), pb2);

        ACLMessage msg2 = new ACLMessage(ACLMessage.REQUEST);
        msg2.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        msg2.setSender(getAID());
        msg2.setLanguage(codec.getName());
        msg2.setOntology(ontology.getName());
        msg2.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));
        msg2.addReceiver(phf.getJ2().getAgenteJugador());
        manager.fillContent(msg2, accion2);
        addBehaviour(new TareaPedirBarcos(this, msg2));
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }

    public Peticion getProximaPeticion() {
        return peticionesCola.poll();
    }

    public void colaPeticion(Peticion peticion) { //este metodo solo es llamado desde la tarea pedir movimiento
        peticionesCola.add(peticion);
        setOcupado(false);//liberamos el agente
    }

    public void pedirMovimiento(Peticion peticion) throws Codec.CodecException, OntologyException {

        String idPartida = peticion.getIdPartida();
        AID proximoLanzador = peticion.getJugadorActivo();
        //Contenido del mensaje
        Jugador lanzador = null;
        PartidaHundirLaFlota partidaActual = tableros.get(idPartida);
        if (partidaActual.getJ1().getAgenteJugador().compareTo(proximoLanzador) == 0) {
            lanzador = partidaActual.getJ1();
        } else if (partidaActual.getJ2().getAgenteJugador().compareTo(proximoLanzador) == 0) {
            lanzador = partidaActual.getJ2();
        }

        PedirMovimiento peticionMov = new PedirMovimiento(partidas.get(idPartida), lanzador);

        //Mensaje a enviar
        ACLMessage msg = new ACLMessage(ACLMessage.CFP);
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
        msg.setSender(getAID());
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology.getName());
        msg.setReplyByDate(new Date(System.currentTimeMillis() + TIME_OUT));

        msg.addReceiver(tableros.get(idPartida).getJ1().getAgenteJugador());
        msg.addReceiver(tableros.get(idPartida).getJ2().getAgenteJugador());

        //Codificamos el mensaje
        Action ac = new Action(this.getAID(), peticionMov);
        manager.fillContent(msg, ac);
         this.addBehaviour(new TareaPedirMovimiento(this, msg));

    }

    @Override
    public ContentManager getManager(Vocabulario.TipoJuego tipoJuego) {
        return manager;
    }

    @Override
    public ContentManager getManager(String nameOntology) {
        return manager;
    }

    @Override
    public Ontology getOntology(Vocabulario.TipoJuego tipoJuego) {
        return ontology;
    }

    @Override
    public Ontology getOntology(String nameOntology) {
        return ontology;
    }

    @Override
    public void addMsgConsola(String msg) {

    }

    @Override
    public void setRegistroJuego(String idJuego, RegistroJuego registro) {

    }

    @Override
    public RegistroJuego getRegistroJuego(String idJuego) {
        return null;
    }

    @Override
    public void addJuego(Juego juego, Organizador agenteOrganizador) {

    }

    public TipoCasilla aplicarMovimiento(MovimientoEntregado movimiento) throws Codec.CodecException, OntologyException {
        movimientosCola.add(movimiento);
        PartidaHundirLaFlota phf = tableros.get(movimiento.getPartida().getIdPartida());
        TipoCasilla resultado = phf.lanzarTorpedo(movimiento.getMovimiento());
        return resultado;
    }

    @Override
    public void pintarTablero(MovimientoEntregado movimiento) {
        PartidaHundirLaFlota partida = tableros.get(movimiento.getPartida().getIdPartida());
        ATableroJFrame tablero = ifaces.get(movimiento.getPartida().getIdPartida());
        Torpedo torpedo = (Torpedo) movimiento.getMovimiento().getFicha();
        Jugador lanzador = torpedo.getLanzador();
        tablero.lanzarTorpedo(lanzador, movimiento.getMovimiento().getPosicion());
    }

    @Override
    public MovimientoEntregado getMovimiento() {
        if (movimientosCola.size() > 0) {
            return movimientosCola.poll();
        } else {
            return null;
        }
    }

    public void colocarBarcos(BarcosColocados barcosC) {
        PartidaHundirLaFlota partida = tableros.get(barcosC.getIdPartida());
        partida.colocarBarcos(barcosC);
        if (partida.partidaInicializada()) {
            ifaces.put(partida.getIdPartida(), new ATableroJFrame(this, partida));
            peticionesCola.add(new Peticion(partida.getIdPartida(), partida.getJ1().getAgenteJugador()));
            partida.printT1();
            System.out.println("-----------------------");
            partida.printT2();
        }

    }

}
